//css_import Universe;

using FellSky.Core;

namespace FellSky {
	public static partial class Script {
	
		// !!! THIS IS A REQUIRED METHOD, CALLED BY THE FELL SKY CORE. !!!
		public static void Initialize() {
		
			Universe.LoadDefault=CreateUniverse;
			
		}
	
	}
}