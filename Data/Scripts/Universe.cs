using FellSky.Core;

namespace FellSky {
	public static partial class Script {

		public static void CreateUniverseFactions(Universe universe) {
			var independent = new Faction
			{
				Name = "Independent",
				Description =
					"Countless freelance ship pilots and captains of no apparent allegiance " +
					"navigate the gravity currents of Sky. Driven by the call of freedom, "+
					"they represent the fierce independent spirit so prevalent among Sky's " +
					"inhabitants.",
			};

			var pirate = new Faction
			{
				Name="Pirate", 
				Description=
					"Driven either by necessity or simply by pure greed and malice, Sky's " +
					"criminal elements prey on the naive and defenseless for fun and profit. " +
					"While some of these outcasts have banded together to form syndicates and " +
					"smuggling rings, some remain independent and prowl the trade routes in search " +
					"of fat, easy freighters to prey on."
			};

			var conglomerate = new Faction()
			{
				Name="Sky Trade Conglomerate",
				Description = 
					"The Sky Trade Conglomerate is a group of megacorporations and quasi-political " +
					"entities who govern a major part of Sky's known territory. They are generally " +
					"peaceful and cooperative when it serves their best interests. Controlling much " +
					"of Sky's economy, they are a powerful force in the economic and political arena."
			};

			var conglomerateforce = new Faction()
			{
				Name="STC Space Forces",
				Description = 
					"The STC Space Forces is a member corporation of the Sky Trade Conglomerate, " +
					"and also functions as its police force. Using state-of-the-art equipment, " +
					"they protect the STC against internal and external threats. They are rather " +
					"small in number when compared to other space militaries, since the members " +
					"of the STC are quite aware and guarded against the possibility of a coup " +
					"within their ranks."
			};

			var junocollective = new Faction()
			{
				Name="Juno Food Collective",
				Description = 
					"The Juno Food Collective is a group of zero-g farmers, meat producers and " +
					"food processing companies. They are a member of the Sky Trade Conglomerate," +
					"and serve as the STC's primary food supplier. They are more accomodating than the " + 
					"other STC members with regards to with whom they conduct business with, thus "+
					"making them a good point of contact for captains and pilots of low reputation with the STC."
			};

			var coremining = new Faction()
			{
				Name="Zeta Core Mining, Inc.",
				Description = 
					"The Zeta Core Mining Incorporated is a major asteroid mining and prospecting "+
					"company in the Sky Trage Conglomerate. They supply the bulk of the STC's "+
					"raw material needs with minerals mined from asteroids and dead planets. " +
					"They are always on the lookout for new mining sites, and will pay a handsome " +
					"reward for a good vein of raw ore."
			};

			var gasmining = new Faction()
			{
				Name="Saturn Gas Extraction, Inc.",
				Description = 
					"The Saturn Gas Extraction Incorporated is a gas giant mining company "+
					"that has its roots at the Sol ringed planet, Saturn. A member of the Sky " +
					"Trade conglomerate, they mine hydrogen, tritium and other rare gases from " +
					"within Sky's voluminous gas clouds. They supply the STC with much needed " +
					"starship and industrial fuels."
			};

			var hiveMolkar = new Faction()
			{
				Name = "Molkar Hive",
				Description = "The Molkar Hive is the first Swarm Hive encountered by humanity " +
							  "inside the cloud. Little is known about the Swarm Hives and there " +
							  "is precious little to learn, since the violent and deadly clashes " +
							  "between human and Swarm leave little live captives for study."
			};

			conglomerate.SetSameRelationship(gasmining,DiplomaticRelationship.Member);
			conglomerate.SetSameRelationship(coremining, DiplomaticRelationship.Member);
			conglomerate.SetSameRelationship(junocollective, DiplomaticRelationship.Member);
			conglomerate.SetSameRelationship(conglomerateforce, DiplomaticRelationship.Member);

			pirate.SetSameRelationship(conglomerate,DiplomaticRelationship.Hostile);
			pirate.SetSameRelationship(coremining, DiplomaticRelationship.Hostile);
			pirate.SetSameRelationship(gasmining, DiplomaticRelationship.Hostile);
			pirate.SetSameRelationship(junocollective, DiplomaticRelationship.Wary);
			pirate.SetSameRelationship(conglomerateforce, DiplomaticRelationship.Hostile);

			hiveMolkar.SetSameRelationship(conglomerate,DiplomaticRelationship.VeryHostile);
			hiveMolkar.SetSameRelationship(pirate, DiplomaticRelationship.VeryHostile);
			hiveMolkar.SetSameRelationship(gasmining, DiplomaticRelationship.VeryHostile);
			hiveMolkar.SetSameRelationship(junocollective, DiplomaticRelationship.VeryHostile);
			hiveMolkar.SetSameRelationship(conglomerateforce, DiplomaticRelationship.VeryHostile);

			universe.Factions.AddRange(new []
			{
			   conglomerate,
			   conglomerateforce,
			   pirate,
			   gasmining,
			   coremining,
			   junocollective,
			   hiveMolkar
			});
		}

		public static Universe CreateUniverse() {
			var universe=new Universe();
			CreateUniverseFactions(universe);
			return universe;
		}
	}
}