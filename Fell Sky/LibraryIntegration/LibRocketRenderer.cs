﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FellSky.AssetManagement;
using LibRocket;
using SFML.Graphics;
using Tao.OpenGl;
using Vertex = LibRocket.Vertex;

namespace FellSky.LibraryIntegration
{
    internal class LibRocketRenderer : RenderInterface
    {
        private readonly Dictionary<IntPtr, Texture> _textures = new Dictionary<IntPtr, Texture>();

        public delegate void UIDrawDelegate();
        public static event UIDrawDelegate OnAfterGUIDraw;

        public static RenderWindow RenderWindow;

        public LibRocketRenderer(RenderWindow window)
        {
            RenderWindow = window;
        }

        public static void Draw()
        {
            RenderWindow.SetActive(true);
            var view = RenderWindow.View;
            RenderWindow.ResetGLStates();
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glPushMatrix();
            Gl.glMatrixMode(Gl.GL_TEXTURE);
            Gl.glLoadIdentity();
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Gl.glOrtho(0, RenderWindow.Size.X, RenderWindow.Size.Y, 0, -1, 1);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            LibRocket.Core.DefaultContext.Render();
            Gl.glDisable(Gl.GL_SCISSOR_TEST);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glPopMatrix();
            RenderWindow.ResetGLStates();
            RenderWindow.View = view;

            if (OnAfterGUIDraw != null) OnAfterGUIDraw();
        }

        public override unsafe void RenderGeometry(Vertex* vertices, Int32 numVertices, Int32* indices, Int32 numIndices,
                                            IntPtr texture, Point translation)
        {
            OpenGLRenderGeometry(vertices, numVertices, indices, numIndices, texture, translation);
        }

        public override void EnableScissorRegion(Boolean enable)
        {
            OpenGLEnableScissorRegion(enable);
        }

        public override void SetScissorRegion(Int32 x, Int32 y, Int32 width, Int32 height)
        {
            OpenGLSetScissorRegion(x, (Int32)RenderWindow.Size.Y - (y + height), width, height);
            //OpenGLSetScissorRegion(x, y, width, height);
        }

        public override void ReleaseTexture(IntPtr texture)
        {
            _textures.Remove(texture);
        }

        public override Boolean LoadTexture(ref IntPtr textureHandle, ref Point dimensions, String source)
        {
            try
            {
                //source = Path.GetFileNameWithoutExtension(source);
                var tex = Asset<Texture>.Assets.ContainsKey(source) ? Asset<Texture>.Get(source) : new Texture(source);
                
                textureHandle = new IntPtr(tex.GetHashCode());
                _textures[textureHandle] = tex;
                dimensions.X = tex.Size.X;
                dimensions.Y = tex.Size.Y;
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public override unsafe Boolean GenerateTexture(ref IntPtr textureHandle, Byte* source, Point dimensions)
        {
            //SFML.Graphics.Image image = new SFML.Graphics.Image((uint)dimensions.X,(uint)dimensions.Y,SFML.Graphics.Color.Blue);
            //System.Runtime.InteropServices.Marshal.Copy(image.Pixels,0,new IntPtr((void *)source),(int)dimensions.X*(int)dimensions.Y*4);
            // 
            var tex = new Texture((uint)dimensions.X, (uint)dimensions.Y);
            var b = new Byte[(uint)dimensions.X * (uint)dimensions.Y * 4];

            var ptr = new IntPtr(source);
            Marshal.Copy(ptr, b, 0, (int)dimensions.X * (int)dimensions.Y * 4);

            tex.Update(b);
            textureHandle = new IntPtr(tex.GetHashCode());
            _textures[textureHandle] = tex;
            return true;
        }

        public override void OpenGLBindTexture(IntPtr texture)
        {
            Texture.Bind(_textures[texture], Texture.CoordinateType.Normalized);
        }
    }
}