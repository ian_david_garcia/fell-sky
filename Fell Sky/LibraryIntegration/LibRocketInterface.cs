﻿using System;
using System.IO;
using CSScriptLibrary;
using LibRocket;
using SFML;
using SFML.Graphics;


namespace FellSky.LibraryIntegration
{
    internal static class LibRocketInterface
    {
        private static bool _debuggerEnabled;
        private static string _rmlScriptHostTemplate;
        private static int _scriptHostCount;
        public static string RmlScriptTemplateFile = "UI/ScriptTemplate.cs";
        public static string RmlScriptNameSpace = "FellSky.Scripting";
        public static string RmlScriptClassPrefix = "RmlScriptHost";
        public static string RmlScriptMethod = "OnDocumentLoad";
        public static Keyboard.Key UIDebugKey= Keyboard.Key.F12;

        public static void Initialize(RenderWindow window)
        {
            LibRocket.Core.Initialize(new LibRocketRenderer(window), (int) window.Size.X, (int) window.Size.Y);
            LibRocket.Core.OnScriptLoad += (o, e) =>
            {
                if (string.IsNullOrWhiteSpace(e.Script)) return;

                if (string.IsNullOrWhiteSpace(_rmlScriptHostTemplate)) _rmlScriptHostTemplate = File.ReadAllText(RmlScriptTemplateFile);
                var className = RmlScriptClassPrefix + _scriptHostCount;
                var str =
                    _rmlScriptHostTemplate.Replace("<<CLASSNAME>>", className)
                                          .Replace("<<SCRIPTCODE>>", e.Script);

                try
                {
                    var asm = new AsmHelper(CSScript.LoadCode(str));
                    asm.Invoke(string.Format("{0}.{1}.{2}", RmlScriptNameSpace, className, RmlScriptMethod), new object[] { e.Document });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                _scriptHostCount++;
            };
            window.OnTextEntered +=
                (o, e) => LibRocket.Core.DefaultContext.ProcessTextInput(Char.ConvertFromUtf32((int)e.Unicode));

            window.OnKeyPressed += (o, e) =>
            {
                if (e.Code == UIDebugKey)
                {
                    
                    _debuggerEnabled = !_debuggerEnabled;
                    LibRocket.Core.SetDebuggerVisible(_debuggerEnabled);
                    Console.WriteLine("Librocket debugger {0}.",_debuggerEnabled?"enabled":"disabled");
                }
                LibRocket.Core.DefaultContext.ProcessKeyDown(TranslateKey(e.Code), GetKeyModifiers());
            };

            window.OnKeyReleased +=
                (o, e) => LibRocket.Core.DefaultContext.ProcessKeyUp(TranslateKey(e.Code), GetKeyModifiers());


            window.OnMouseDown +=
                (o, e) => LibRocket.Core.DefaultContext.ProcessMouseButtonDown((int)e.Button, GetKeyModifiers());

            window.OnMouseUp +=
                (o, e) => LibRocket.Core.DefaultContext.ProcessMouseButtonUp((int)e.Button, GetKeyModifiers());

            window.OnMouseWheel += (o, e) => LibRocket.Core.DefaultContext.ProcessMouseWheel(-e.Delta, GetKeyModifiers());

            window.OnMouseMove += (o, e) => LibRocket.Core.DefaultContext.ProcessMouseMove(e.X, e.Y, GetKeyModifiers());
        }

        private static int GetKeyModifiers()
        {
            var modifiers = 0;

            if (Keyboard.IsKeyPressed(Keyboard.Key.LShift) ||
                Keyboard.IsKeyPressed(Keyboard.Key.RShift))
                modifiers |= (int)KeyModifier.KM_SHIFT;

            if (Keyboard.IsKeyPressed(Keyboard.Key.LControl) ||
                Keyboard.IsKeyPressed(Keyboard.Key.RControl))
                modifiers |= (int)KeyModifier.KM_CTRL;

            if (Keyboard.IsKeyPressed(Keyboard.Key.LAlt) ||
                Keyboard.IsKeyPressed(Keyboard.Key.RAlt))
                modifiers |= (int)KeyModifier.KM_ALT;

            return modifiers;
        }

        private static KeyIdentifier TranslateKey(Keyboard.Key key)
        {
            switch (key)
            {
                case Keyboard.Key.A:
                    return KeyIdentifier.KI_A;
                case Keyboard.Key.B:
                    return KeyIdentifier.KI_B;

                case Keyboard.Key.C:
                    return KeyIdentifier.KI_C;

                case Keyboard.Key.D:
                    return KeyIdentifier.KI_D;

                case Keyboard.Key.E:
                    return KeyIdentifier.KI_E;

                case Keyboard.Key.F:
                    return KeyIdentifier.KI_F;

                case Keyboard.Key.G:
                    return KeyIdentifier.KI_G;

                case Keyboard.Key.H:
                    return KeyIdentifier.KI_H;

                case Keyboard.Key.I:
                    return KeyIdentifier.KI_I;

                case Keyboard.Key.J:
                    return KeyIdentifier.KI_J;

                case Keyboard.Key.K:
                    return KeyIdentifier.KI_K;

                case Keyboard.Key.L:
                    return KeyIdentifier.KI_L;

                case Keyboard.Key.M:
                    return KeyIdentifier.KI_M;

                case Keyboard.Key.N:
                    return KeyIdentifier.KI_N;

                case Keyboard.Key.O:
                    return KeyIdentifier.KI_O;

                case Keyboard.Key.P:
                    return KeyIdentifier.KI_P;

                case Keyboard.Key.Q:
                    return KeyIdentifier.KI_Q;

                case Keyboard.Key.R:
                    return KeyIdentifier.KI_R;

                case Keyboard.Key.S:
                    return KeyIdentifier.KI_S;

                case Keyboard.Key.T:
                    return KeyIdentifier.KI_T;

                case Keyboard.Key.U:
                    return KeyIdentifier.KI_U;

                case Keyboard.Key.V:
                    return KeyIdentifier.KI_V;

                case Keyboard.Key.W:
                    return KeyIdentifier.KI_W;

                case Keyboard.Key.X:
                    return KeyIdentifier.KI_X;

                case Keyboard.Key.Y:
                    return KeyIdentifier.KI_Y;

                case Keyboard.Key.Z:
                    return KeyIdentifier.KI_Z;

                case Keyboard.Key.Num0:
                    return KeyIdentifier.KI_0;

                case Keyboard.Key.Num1:
                    return KeyIdentifier.KI_1;

                case Keyboard.Key.Num2:
                    return KeyIdentifier.KI_2;

                case Keyboard.Key.Num3:
                    return KeyIdentifier.KI_3;

                case Keyboard.Key.Num4:
                    return KeyIdentifier.KI_4;

                case Keyboard.Key.Num5:
                    return KeyIdentifier.KI_5;

                case Keyboard.Key.Num6:
                    return KeyIdentifier.KI_6;

                case Keyboard.Key.Num7:
                    return KeyIdentifier.KI_7;

                case Keyboard.Key.Num8:
                    return KeyIdentifier.KI_8;

                case Keyboard.Key.Num9:
                    return KeyIdentifier.KI_9;

                case Keyboard.Key.Numpad0:
                    return KeyIdentifier.KI_NUMPAD0;

                case Keyboard.Key.Numpad1:
                    return KeyIdentifier.KI_NUMPAD1;

                case Keyboard.Key.Numpad2:
                    return KeyIdentifier.KI_NUMPAD2;

                case Keyboard.Key.Numpad3:
                    return KeyIdentifier.KI_NUMPAD3;

                case Keyboard.Key.Numpad4:
                    return KeyIdentifier.KI_NUMPAD4;

                case Keyboard.Key.Numpad5:
                    return KeyIdentifier.KI_NUMPAD5;

                case Keyboard.Key.Numpad6:
                    return KeyIdentifier.KI_NUMPAD6;

                case Keyboard.Key.Numpad7:
                    return KeyIdentifier.KI_NUMPAD7;

                case Keyboard.Key.Numpad8:
                    return KeyIdentifier.KI_NUMPAD8;

                case Keyboard.Key.Numpad9:
                    return KeyIdentifier.KI_NUMPAD9;

                case Keyboard.Key.Left:
                    return KeyIdentifier.KI_LEFT;

                case Keyboard.Key.Right:
                    return KeyIdentifier.KI_RIGHT;

                case Keyboard.Key.Up:
                    return KeyIdentifier.KI_UP;

                case Keyboard.Key.Down:
                    return KeyIdentifier.KI_DOWN;

                case Keyboard.Key.Add:
                    return KeyIdentifier.KI_ADD;

                case Keyboard.Key.Back:
                    return KeyIdentifier.KI_BACK;

                case Keyboard.Key.Delete:
                    return KeyIdentifier.KI_DELETE;

                case Keyboard.Key.Divide:
                    return KeyIdentifier.KI_DIVIDE;

                case Keyboard.Key.End:
                    return KeyIdentifier.KI_END;

                case Keyboard.Key.Escape:
                    return KeyIdentifier.KI_ESCAPE;

                case Keyboard.Key.F1:
                    return KeyIdentifier.KI_F1;

                case Keyboard.Key.F2:
                    return KeyIdentifier.KI_F2;

                case Keyboard.Key.F3:
                    return KeyIdentifier.KI_F3;

                case Keyboard.Key.F4:
                    return KeyIdentifier.KI_F4;

                case Keyboard.Key.F5:
                    return KeyIdentifier.KI_F5;

                case Keyboard.Key.F6:
                    return KeyIdentifier.KI_F6;

                case Keyboard.Key.F7:
                    return KeyIdentifier.KI_F7;

                case Keyboard.Key.F8:
                    return KeyIdentifier.KI_F8;

                case Keyboard.Key.F9:
                    return KeyIdentifier.KI_F9;

                case Keyboard.Key.F10:
                    return KeyIdentifier.KI_F10;

                case Keyboard.Key.F11:
                    return KeyIdentifier.KI_F11;

                case Keyboard.Key.F12:
                    return KeyIdentifier.KI_F12;

                case Keyboard.Key.F13:
                    return KeyIdentifier.KI_F13;

                case Keyboard.Key.F14:
                    return KeyIdentifier.KI_F14;

                case Keyboard.Key.F15:
                    return KeyIdentifier.KI_F15;

                case Keyboard.Key.Home:
                    return KeyIdentifier.KI_HOME;

                case Keyboard.Key.Insert:
                    return KeyIdentifier.KI_INSERT;

                case Keyboard.Key.LControl:
                    return KeyIdentifier.KI_LCONTROL;

                case Keyboard.Key.LShift:
                    return KeyIdentifier.KI_LSHIFT;

                case Keyboard.Key.Multiply:
                    return KeyIdentifier.KI_MULTIPLY;

                case Keyboard.Key.Pause:
                    return KeyIdentifier.KI_PAUSE;

                case Keyboard.Key.RControl:
                    return KeyIdentifier.KI_RCONTROL;

                case Keyboard.Key.Return:
                    return KeyIdentifier.KI_RETURN;

                case Keyboard.Key.RShift:
                    return KeyIdentifier.KI_RSHIFT;

                case Keyboard.Key.Space:
                    return KeyIdentifier.KI_SPACE;

                case Keyboard.Key.Subtract:
                    return KeyIdentifier.KI_SUBTRACT;

                case Keyboard.Key.Tab:
                    return KeyIdentifier.KI_TAB;
            }
            

            return KeyIdentifier.KI_UNKNOWN;
        }
    }
}