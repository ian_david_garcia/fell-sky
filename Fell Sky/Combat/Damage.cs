﻿using FellSky.Utilities;

namespace FellSky.Combat
{
    public class Damage
    {
        public IDamageSource Source;
        public float Amount { get; set; }
        public int Type { get; set; }


        /// <summary>
        /// Default damage application function
        /// </summary>
        /// <param name="obj"></param>
        public virtual void ApplyTo(IDamageable obj)
        {
            obj.CurrentHealth = (obj.CurrentHealth - Amount).Clamp(0, obj.MaxHealth);
        }
    }
}
