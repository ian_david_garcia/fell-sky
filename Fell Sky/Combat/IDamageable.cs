﻿using System;

namespace FellSky.Combat
{
    public interface IDamageable: IHealth
    {
        /// <summary>
        /// This event gives a chance to modify the incoming damage.
        /// </summary>
        event Action<IDamageable, Damage> BeforeDamage;

        /// <summary>
        /// This event fires after the damage is applied.
        /// </summary>
        event Action<IDamageable, Damage> AfterDamage;
        
        void ApplyDamage(Damage damage);
    }
}
