﻿using System;

namespace FellSky.Combat
{
    /// <summary>
    /// Interface for shields.
    /// Shields are devices that absorb incoming harmful energy and convert it to stored entropy. 
    /// When entropy exceeds the shield's capacity, the shield overloads and shuts down.
    /// A shield when destoyed releases its stored entropy as a combination of heat and EMP.
    /// </summary>
    public interface IShield
    {
        float Capacity { get; set; }
        float Entropy { get; set; }
        float EntropyDispelRate { get; set; }

        event Action<IShield, Damage> BeforeShieldDamage;
        event Action<IShield, Damage> AfterShieldDamage;
        event Action<IShield, Damage> OnShieldOverload;

        bool IsOverloaded { get; }
    }

}
