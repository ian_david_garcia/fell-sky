﻿namespace FellSky.Combat
{
    public interface IDamageSource
    {
        Damage Damage { get; set; }
    }
}
