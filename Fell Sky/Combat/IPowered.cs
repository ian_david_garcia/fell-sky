﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FellSky.Combat
{
    public interface IPowered
    {
        bool IsPowered { get; set; }
        float PowerDraw { get; set; }
    }
}
