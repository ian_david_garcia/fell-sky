﻿namespace FellSky.Combat
{
    public interface IFriendOrFoe
    {
        bool IsFriendlyTo(IFriendOrFoe obj);
    }
}
