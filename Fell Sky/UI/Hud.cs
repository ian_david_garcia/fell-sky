﻿using System.Collections.Generic;
using FellSky.Graphics;
using LibRocket;
using SFML;

namespace FellSky.UI
{
    public class Hud
    {


        private bool _isEnabled = false;
        public LinkedListNode<IRenderable> RenderNode { get; set; }

        public Document MainDocument;

        public void Enable()
        {
            _isEnabled = true;
            MainDocument.Show();
        }

        public void Disable()
        {
            _isEnabled = false;
            MainDocument.Hide();
        }

        public void Initialize()
        {
            Game.RenderWindow.OnKeyPressed += KeyPressHandler;
        }

        private void KeyPressHandler(object sender, Window.Event.KeyEventArgs e)
        {
            if (!_isEnabled) return;
        }

    }
}
