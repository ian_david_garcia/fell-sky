﻿using SFML;

namespace FellSky.UI
{
    public class KeyboardControls
    {
        public Keyboard.Key ShipControlUp = Keyboard.Key.W;
        public Keyboard.Key ShipControlDown = Keyboard.Key.S;
        public Keyboard.Key ShipControlLeft = Keyboard.Key.A;
        public Keyboard.Key ShipControlRight = Keyboard.Key.D;
        public Keyboard.Key ShipControlAltLeft = Keyboard.Key.Q;
        public Keyboard.Key ShipControlAltRight = Keyboard.Key.E;
        public Keyboard.Key ShipDock = Keyboard.Key.Z;

        public Keyboard.Key ScreenShipStatus = Keyboard.Key.F1;
        public Keyboard.Key ScreenMap = Keyboard.Key.F2;
        public Keyboard.Key ScreenInventory = Keyboard.Key.F3;
        public Keyboard.Key ScreenCrew = Keyboard.Key.F4;

    }
}
