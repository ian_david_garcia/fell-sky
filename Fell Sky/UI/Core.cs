﻿using FellSky.LibraryIntegration;
using FellSky.Network;

namespace FellSky.UI
{
    public static class Core
    {
        public static KeyboardControls Controls=new KeyboardControls();


        public static void Initialize()
        {
            LibRocketInterface.Initialize(Game.RenderWindow);
        }

        public static void Update()
        {
            LibRocket.Core.DefaultContext.Update();
        }

        public static void Render()
        {
            LibRocketRenderer.Draw();
        }
    }
}
