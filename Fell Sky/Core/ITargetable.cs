﻿using FellSky.Combat;

namespace FellSky.Core
{
    public interface ITargetable: ISensorContact, ICollidable, IFriendOrFoe
    {
        
    }
}
