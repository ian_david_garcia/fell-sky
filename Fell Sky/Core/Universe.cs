﻿using System;
using System.Collections.Generic;

namespace FellSky.Core
{
    [Serializable]
    public class Universe
    {
        public List<Faction> Factions = new List<Faction>();
        public List<World> Worlds = new List<World>();

        public Universe()
        {
        }

        // script hook
        public static Func<Universe> LoadDefault;
    }
}
