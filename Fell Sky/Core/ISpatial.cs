﻿using SFML;
using SFML.Graphics;

namespace FellSky.Core
{
    public interface ISpatial
    {
        Vector2f Position { get; set; }
        Vector2f Scale { get; set; }
        Vector2f Origin { get; set; }
        float Rotation { get; set; }
        Transform Transform { get; }
        Transform InverseTransform { get; }
    }
}
