﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FellSky.Entities;

namespace FellSky.Core
{
    public interface ISensorContact: IMoveable
    {
        [Category("Sensor")]
        long EmittedSignatures { get; set; }

        event Action<Entity> OnDispose;
        IEnumerable<ISensorContact> GetChildSensorContacts();
    }
}
