﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FellSky.Core
{
    public interface ITagged
    {
        string[] Tags { get; set; }
    }
}
