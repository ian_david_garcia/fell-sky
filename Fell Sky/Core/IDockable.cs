﻿namespace FellSky.Core
{
    public interface IDockable
    {
        IDockable Docking { get; }
    }
}
