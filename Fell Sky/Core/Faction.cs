﻿using System;
using System.Collections.Generic;
using FellSky.Entities;

namespace FellSky.Core
{
    public enum DiplomaticRelationship
    {
        Unknown = 0,        // Object is unknown. Has friendly fire.
        
        Friendly = 1,       // Default for friendly objects. Has friendly fire by default.
        Ally = 2,           // For allies. Will provide aid. No friendly fire by default
        Member = 3,         // For objects of the same faction. No friendly fire by default.

        Wary = -1,          // Default for hostile objects - will shoot on the slightest provocation. Will not provide aid. Friendly fire is on.
        Hostile = -2,       // Will shoot on sight
        VeryHostile = -3,   // Will hunt object down
    }

    public interface IDiplomaticObject
    {
        Dictionary<IDiplomaticObject, DiplomaticRelationship> Relationships { get; set; }
    }

    [Serializable]
    public class Faction: Entity, IDiplomaticObject
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Dictionary<IDiplomaticObject, DiplomaticRelationship> Relationships { get; set; }

        public Faction()
        {
            Relationships=new Dictionary<IDiplomaticObject, DiplomaticRelationship>();
        }

        public void SetSameRelationship(IDiplomaticObject obj, DiplomaticRelationship relationship)
        {
            Relationships[obj] = relationship;
            obj.Relationships[this] = relationship;
        }
    }


}
