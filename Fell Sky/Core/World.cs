﻿using System;
using System.Collections.Generic;
using FellSky.Entities;
using FellSky.Graphics;
using SFML;
using SFML.Graphics;

namespace FellSky.Core
{
    public enum RenderPosition
    {
        Before, After, First, Last
    }

    public class World: IUpdateable
    {
        public Camera Camera { get; set; }

        public LinkedList<Entity> Entities = new LinkedList<Entity>();

        public event Action<Entity> OnAddEntity;
        public event Action<Entity> OnRemoveEntity;
        public event Action<TimeSpan> OnUpdate;

        public void AddEntity(Entity entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            if(entity.Status==EntityStatus.Spawned) throw new ArgumentException("Cannot spawn entity: entity already spawned.");
            entity.WorldNode = Entities.AddLast(entity);
            entity.Spawn(this);
            if (OnAddEntity != null) OnAddEntity(entity);
        }

        public void RemoveEntity(Entity entity)
        {
            if(entity==null) throw new ArgumentNullException("entity");
            if (entity.Status != EntityStatus.Spawned)
                throw new InvalidOperationException("Cannot remove entity: object not spawned.");
            if (entity.WorldNode == null || entity.WorldNode.List != Entities)
                throw new InvalidOperationException("Cannot remove entity: invalid WorldNode.");
            entity.Dispose();
            if (OnRemoveEntity != null) OnRemoveEntity(entity);
        }

        public virtual void Update(TimeSpan timeStep)
        {
            var node = Entities.First;
            while (node != null)
            {
                var thisNode = node;
                node = node.Next;
                var obj = thisNode.Value;
                switch (obj.Status)
                {
                    case EntityStatus.Spawned:
                        obj.Update(timeStep);
                        break;
                    case EntityStatus.Disposed:
                        RemoveEntity(obj);
                        break;
                    case EntityStatus.Invalid:
                        throw new Exception("How did an invalid object get inside the world?!?");
                }
            }

            if (OnUpdate != null) OnUpdate(timeStep);
        }

        private class RenderLayer : IRenderable
        {
            public LinkedListNode<IRenderable> RenderNode { get; set; }
            public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnBeforeRender;
            public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnAfterRender;

            public void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states, int renderMode = 0)
            {
                if (OnBeforeRender != null) OnBeforeRender(this, timeSpan, target, camera, states,renderMode);
                if (OnAfterRender != null) OnAfterRender(this, timeSpan, target, camera, states,renderMode);
            }
        }

        private const int MaxLayers = 20;

        private readonly LinkedList<IRenderable> _renderList = new LinkedList<IRenderable>();

        private readonly LinkedListNode<IRenderable>[] _layers = new LinkedListNode<IRenderable>[MaxLayers];

        public World()
        {
            Camera = new Camera();

            for (var i = 0; i < _layers.Length; i++)
                _layers[i] = _renderList.AddLast(new RenderLayer());
        }


        /// <summary>
        /// Add a renderable to the renderlist.
        /// </summary>
        /// <param name="entity">Entity to render.</param>
        /// <param name="pos">Relative ordering of object inside the renderlist</param>
        /// <param name="relativeTo">Reference object for RenderPosition.Before and RenderPosition.After</param>
        public void AddRenderable(IRenderable entity, RenderPosition pos = RenderPosition.Last,
            IRenderable relativeTo = null)
        {
            switch (pos)
            {
                case RenderPosition.Before:
                    if (relativeTo == null || relativeTo.RenderNode.List != _renderList) throw new ArgumentException("Invalid relative IRenderable.");
                    entity.RenderNode = _renderList.AddBefore(relativeTo.RenderNode, entity);
                    break;
                case RenderPosition.After:
                    if (relativeTo == null || relativeTo.RenderNode.List != _renderList) throw new ArgumentException("Invalid relative IRenderable.");
                    entity.RenderNode = _renderList.AddAfter(relativeTo.RenderNode, entity);
                    break;
                case RenderPosition.First:
                    entity.RenderNode = _renderList.AddFirst(entity);
                    break;
                case RenderPosition.Last:
                    entity.RenderNode = _renderList.AddLast(entity);
                    break;
            }
        }

        public void RemoveRenderable(IRenderable obj)
        {
            if (obj.RenderNode.List != _renderList) return;
            _renderList.Remove(obj.RenderNode);
        }

        public void Render(RenderTarget target, TimeSpan timeSpan)
        {
            foreach (var obj in _renderList)
            {
                obj.Render(timeSpan,target,Camera,RenderStates.Default);
            }
        }


    }
}
