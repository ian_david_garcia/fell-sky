﻿using System;

namespace FellSky.Core
{

    public interface IUpdateable
    {
        event Action<TimeSpan> OnUpdate;
        void Update(TimeSpan timeStep);
    }
}
