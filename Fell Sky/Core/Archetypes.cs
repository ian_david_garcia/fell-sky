﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;


namespace FellSky.Core
{
    public static class Archetypes<T> where T: ICloneable, IArchetype
    {
        private static Dictionary<string, T> ArchetypeList = new Dictionary<string,T>();
        private static readonly BinaryFormatter Formatter = new BinaryFormatter();

        public static void LoadArchetypes(string filename)
        {
            try
            {
                var stream = File.OpenRead(filename);
                ArchetypeList = (Dictionary<string, T>) Formatter.Deserialize(stream);
                stream.Close();
            }
            catch
            {
                Console.WriteLine(
                    "[Archetypes<{0}>.LoadArchetypeEntries] Unable to load archetype entries from {1}.", typeof(T), filename);
            }
        }

        public static void SaveArchetypes(string filename)
        {
            try
            {
                var stream = File.OpenWrite(filename);
                Formatter.Serialize(stream,ArchetypeList);
            }
            catch
            {
                Console.WriteLine(
                    "[Archetypes<{0}>.LoadArchetypeEntries] Unable to save archetype entries to {1}.", typeof(T), filename);
            }
        }


        public static void RemoveArchetype(string archetypeID)
        {
            ArchetypeList.Remove(archetypeID);
        }

        public static T CreateObject(string archetype)
        {
            try
            {
                return (T)ArchetypeList[archetype].Clone();
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException(string.Format("[Archetypes<{0}>.CreateObject] No archetype of name \"{1} exists.\"",typeof(T), archetype),
                    "archetype");
            }
        }

        public class StandardValueConverter : TypeConverter
        {
            public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(ArchetypeList.Keys.ToArray());
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }
        }
    }
}
