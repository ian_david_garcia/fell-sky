﻿using System;


namespace FellSky.Core
{
    public interface IArchetype: ICloneable
    {
        string ArchetypeID { get; set; }
    }
}
