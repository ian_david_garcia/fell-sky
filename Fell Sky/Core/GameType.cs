﻿namespace FellSky.Core
{
    public enum GameType
    {
        Sandbox,
        FreeForAll,
        FleetDeathmatch,
        FortressDeathmatch,
        Survival,
    }
}