﻿using SFML;

namespace FellSky.Core
{
    public interface IMoveable: ISpatial
    {
        Vector2f LinearVelocity { get; set; }
        float AngularVelocity { get; set; }
    }
}