﻿using System.ComponentModel;


namespace FellSky.Core
{
    public interface ISensor: ISpatial
    {
        [Browsable(false)]
        ISensorContact CurrentScannedContact { get; }

        [Category("Sensor")]
        float SensorSensitivity { get; }
        [Category("Sensor")]
        int DetectedSignatures { get; }

        void DetectObject(ISensorContact contact);
    }
}
