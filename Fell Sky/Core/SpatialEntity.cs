﻿using System;
using FellSky.Entities;
using SFML;
using SFML.Graphics;

namespace FellSky.Core
{
    [Serializable]
    public class SpatialEntity: Entity, ISpatial
    {
        [Flags]
        public enum TransformChanges
        {
            Position=1>>0, Scale=1>>1, Rotation=1>>2, Origin=1>>3
        }
        [NonSerialized]
        public TransformChanges TransformChanged = TransformChanges.Position | TransformChanges.Scale | TransformChanges.Rotation | TransformChanges.Origin;
        private Vector2f _position;
        private Vector2f _scale;
        private float _rotation;
        [NonSerialized]
        private Transform _transform;
        [NonSerialized]
        private Transform _inverseTransform;
        private Vector2f _origin;
        [NonSerialized]
        private bool _inverseInvalid = true;

        public SpatialEntity()
        {
            _scale=new Vector2f(1,1);
        }

        public virtual Vector2f Position
        {
            get { return _position; }
            set
            {
                TransformChanged = TransformChanged | TransformChanges.Position;
                _inverseInvalid = true;
                _position = value;
            }
        }

        public virtual Vector2f Origin
        {
            get { return _origin; }
            set
            {
                TransformChanged = TransformChanged | TransformChanges.Origin;
                _inverseInvalid = true;
                _origin = value;
            }
        }

        public virtual Vector2f Scale
        {
            get { return _scale; }
            set
            {
                TransformChanged = TransformChanged | TransformChanges.Scale;
                _inverseInvalid = true;
                _scale = value;
            }
        }

        public virtual float Rotation
        {
            get { return _rotation;  }
            set
            {
                TransformChanged = TransformChanged | TransformChanges.Rotation;
                _inverseInvalid = true;
                _rotation = value;
            }
        }

        public Transform Transform
        {
            get
            {
                if (TransformChanged == 0) return _transform;
                var num1 = (float)(-(double)_rotation * 3.14159274101257 / 180.0);
                var num2 = (float)Math.Cos(num1);
                var num3 = (float)Math.Sin(num1);
                var a00 = _scale.X * num2;
                var a11 = _scale.Y * num2;
                var num4 = _scale.X * num3;
                var a01 = _scale.Y * num3;
                var num5 = (double)_position.X;
                var num6 = _origin.Y * (double)a01;
                var a02 = (float)(num5 - (_origin.X * (double)a00 + num6));
                var a12 = _position.Y + (float)(_origin.X * (double)num4 - _origin.Y * (double)a11);
                _transform = new Transform(a00, a01, a02, -num4, a11, a12, 0.0f, 0.0f, 1f);
                TransformChanged = 0;

                return _transform;
            }
        }

        public Transform InverseTransform
        {
            get
            {
                if (!_inverseInvalid) return _inverseTransform;
                _inverseInvalid = false;
                _inverseTransform = _transform.Inverse;
                return _inverseTransform;
            }
        }
    }
}
