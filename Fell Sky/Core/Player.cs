﻿using System.Net;
using FellSky.Network;
using FellSky.Network.Messages;

namespace FellSky.Core
{
    public class Player
    {
        public int ID;
        public string Name;
        public string CurrentShipArchetype;

        public bool IsReady = false;

        public IPEndPoint Address;

        public Player()
        {
        }

        public Player(TryJoinGameMessage joinMsg)
        {
            ID = GetHashCode();
            Name = joinMsg.Username;
            Address = joinMsg.Address;
        }
    }
}
