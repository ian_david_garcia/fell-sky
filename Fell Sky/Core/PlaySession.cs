﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;

namespace FellSky.Core
{
    public abstract class PlaySession
    {
        
        public List<Player> Players = new List<Player>();

        public Dictionary<Vector2i,World> Worlds = new Dictionary<Vector2i, World>();

        public abstract bool Start();
    }
}
