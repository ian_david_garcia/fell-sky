﻿using System;
using FellSky.Network;
using Lidgren.Network;

namespace FellSky.Core
{
    public sealed class EntitySpawner<TU>: INetworkListener where TU:INetMessage, new()
    {
        private static EntitySpawner<TU> _instance;

        private readonly TU _message = new TU();

        private readonly Action<TU> _spawnEntity;

        private EntitySpawner(Action<TU> spawner)
        {
            _spawnEntity = spawner;
            //Network.Core.Client.RegisterHandler(_message.MessageCode, 0, this);
        } 

       
        public void HandleNetworkMessage(NetMessageHeader header, NetIncomingMessage message)
        {
            _message.ReadFromMessage(message);
            _spawnEntity(_message);
        }

        public static void RegisterEntitySpawner(Action<TU> spawnAction)
        {
            if(_instance!=null) throw new InvalidOperationException("EntitySpawner already registered.");
            _instance = new EntitySpawner<TU>(spawnAction);
        }
    }
}
