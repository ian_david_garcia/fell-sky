﻿using System.Collections.Generic;

namespace FellSky.Core
{
    public interface IParent<out T>
    {
        IEnumerable<T> Children { get; }
    }
}
