﻿using SFML;

namespace FellSky.Core
{
    public interface IChildEntity<out T>
    {
        T Parent { get; }

        Vector2f GlobalPosition { get; set; }
        Vector2f GlobalRotation { get; set; }
        Vector2f GlobalScale { get; set; }
    }
}
