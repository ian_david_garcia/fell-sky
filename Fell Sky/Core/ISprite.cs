﻿using System.ComponentModel;
using FellSky.AssetManagement;
using SFML.Graphics;

namespace FellSky.Core
{
    public interface ISprite
    {
        [TypeConverter(typeof(Asset<Sprite>.StandardValueConverter))]
        string SpriteID { get; set; }

        Sprite Sprite { get; set; }
    }
}
