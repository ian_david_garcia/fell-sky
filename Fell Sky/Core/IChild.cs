﻿namespace FellSky.Core
{
    public interface IChild<T>
    {
        T Parent { get; set; }
    }
}
