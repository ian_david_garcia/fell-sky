﻿using FellSky.Core;
using FellSky.Entities;
using FellSky.EntityComponents;

namespace FellSky.ShipControllers
{
    public interface IShipController: IEntityComponent
    {
        Ship Ship { get; }
    }
}
