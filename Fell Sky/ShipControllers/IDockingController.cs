﻿using System;
using System.Collections.Generic;
using FellSky.Core;
using FellSky.Entities.ShipParts;

namespace FellSky.ShipControllers
{
    public interface IDockingController: IShipController
    {
        List<DockingPort> DockPorts { get; }
        IEnumerable<Tuple<DockingPort,IDockable>> DockedObjects { get; }

        void DockWith(IDockable obj);
        void Undock(DockingPort port);
        void UndockAll();
        
    }
}
