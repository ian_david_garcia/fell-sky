﻿using System;

namespace FellSky.Utilities
{
    public static class MathUtils
    {
        public static float ToDegrees(this float radians)
        {
            return radians * (float)(180.0 / Math.PI);
        }
        
        public static float ToRadians(this float degrees)
        {
            return degrees * (float)(Math.PI / 180.0);
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            return val.CompareTo(min) < 0 ? min : (val.CompareTo(max) > 0 ? max : val);
        }
    }
}
