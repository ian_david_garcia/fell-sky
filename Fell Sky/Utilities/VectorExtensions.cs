﻿using LibRocket;
using SFML;

namespace FellSky.Utilities
{
    public static class VectorExtensions
    {
        public static Point ToLibRocketPoint(this Vector2f v)
        {
            return new Point{X=v.X,Y=v.Y};
        }
    }
}
