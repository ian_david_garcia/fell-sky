﻿using SFML;

namespace FellSky.Utilities
{
    public static class Interpolation
    {
        public static float Lerp(this float v1, float v2, float amount)
        {
            return v1 + (v2 - v1)*amount;
        }

        public static Vector2f Lerp(this Vector2f v1, Vector2f v2, float amount)
        {
            return new Vector2f(v1.X.Lerp(v2.X,amount),v1.Y.Lerp(v2.Y,amount));
        }
    }
}
