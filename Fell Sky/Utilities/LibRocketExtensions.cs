﻿using System;
using System.Collections;
using System.Collections.Generic;
using FellSky.AssetManagement;
using LibRocket;
using SFML.Graphics;

namespace FellSky.Utilities
{
    public static class LibRocket
    {
        private static Element _dummyElement;

        /// <summary>
        /// Creates an <img> element from a sprite id.</img>
        /// </summary>
        /// <param name="spriteID">A valid spriteid.</param>
        /// <returns></returns>
        public static Element CreateSpriteElementFrom(string spriteID)
        {
            if(_dummyElement==null) _dummyElement=new Element("div");
            var asset = ((SpriteAsset) Asset<Sprite>.Assets[spriteID]);
            _dummyElement.InnerRML = string.Format("<img src=\"{0}\" coords=\"{1}px, {2}px, {3}px, {4}px\" />",
                asset.TextureID,
                asset.Value.TextureRect.Left, asset.Value.TextureRect.Top, asset.Value.TextureRect.Right, asset.Value.TextureRect.Bottom
                );
            var elem = _dummyElement.GetChild(0);
            _dummyElement.RemoveChild(elem);
            return elem;
        }

        /// <summary>
        /// Creates an arbitrary tag with a specified decroator and sprite as background.
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="spriteID"></param>
        /// <param name="decoratorname"></param>
        /// <param name="decoratortype"></param>
        /// <returns></returns>
        public static Element CreateBackgroundSpriteElement(string tagName, string spriteID, string decoratorname="bg", string decoratortype="image")
        {
            if (_dummyElement == null) _dummyElement = new Element("div");
            var asset = ((SpriteAsset)Asset<Sprite>.Assets[spriteID]);
            _dummyElement.InnerRML = string.Format("<{0} style=\"{1}-decorator:{7}; {1}-{7}-src:{2};  {1}-{7}-s:{3}px; {1}-{7}-t:{4}px; {1}-{7}-u:{5}px; {1}-{7}-v:{6}px;\" />",
                tagName,
                decoratorname,
                asset.TextureID,
                asset.Value.TextureRect.Left, asset.Value.TextureRect.Top, asset.Value.TextureRect.Right, asset.Value.TextureRect.Bottom,
                decoratortype
                );

            var elem = _dummyElement.GetChild(0);
            _dummyElement.RemoveChild(elem);
            return elem;
        }

        public static void SetText(this Element elem, string text)
        {
            elem.InnerRML = text.Replace(">", "&gt;").Replace("<", "&lt;");
        }

        public static ElementCollection GetChildren(this Element elem)
        {
            return new ElementCollection(elem);
        }

        public class ElementCollection : IEnumerable<Element>
        {
            public class ElementEnumerator : IEnumerator<Element>
            {
                private readonly Element _element;
                private int _index;

                public ElementEnumerator(Element element)
                {
                    _element = element;
                    _index = -1;
                }

                public void Dispose(){}

                public bool MoveNext()
                {
                    _index++;
                    return _index >= 0 && _index < _element.NumChildren;
                }

                public void Reset()
                {
                    _index = -1;
                }

                public Element Current
                {
                    get
                    {
                        if (_index >= 0 && _index < _element.NumChildren) return _element.GetChild(_index);
                        throw new InvalidOperationException();
                    }
                }

                object IEnumerator.Current
                {
                    get { return Current; }
                }
            }

            private readonly Element _element;

            public ElementCollection(Element element)
            {
                _element = element;
            }


            public IEnumerator<Element> GetEnumerator()
            {
                return new ElementEnumerator(_element);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return new ElementEnumerator(_element);
            }


            // batch manipulators
            public void SetProperty(string index, string value)
            {
                foreach (var elem in this)
                    elem.SetProperty(index, value);
            }

            public void SetProperty(string index, global::LibRocket.Color value)
            {
                foreach (var elem in this)
                    elem.SetProperty(index, value);
            }

            public void SetProperty(string index, float value)
            {
                foreach (var elem in this)
                    elem.SetProperty(index, value);
            }

            public void SetProperty(string index, string value, Element.PropertyType type)
            {
                foreach (var elem in this)
                    elem.SetProperty(index, value, type);
            }

            public void SetProperty(string index, global::LibRocket.Color value, Element.PropertyType type)
            {
                foreach (var elem in this)
                    elem.SetProperty(index, value, type);
            }

            public void SetProperty(string index, float value, Element.PropertyType type)
            {
                foreach (var elem in this)
                    elem.SetProperty(index, value, type);
            }

            public void SetAttribute(string index, string value)
            {
                foreach (var elem in this)
                    elem.SetAttribute(index,value);
            }

            public void SetAttribute(string index, float value)
            {
                foreach (var elem in this)
                    elem.SetAttribute(index,value);
            }
        }
    }
}
