﻿using System;
using System.Collections.Generic;
using System.Xml;
using Lidgren.Network;
using SFML;

namespace FellSky.Utilities
{
    public static class MiscExtensions
    {
        public static void Log(this object obj, string str)
        {
            Console.WriteLine("[{0}] {1}", obj.GetType(), str);
        }

        public static void Log(this object obj, string str, params object[] args)
        {
            if (args == null) throw new ArgumentNullException("args");
            Console.WriteLine("[{0}] {1}", obj.GetType(), string.Format(str,args));
        }

        public static void Write(this NetOutgoingMessage msg, Vector2f v)
        {
            msg.Write(v.X);
            msg.Write(v.Y);
        }

        // ReSharper disable once InconsistentNaming
        public static Vector2f ReadVector2f(this NetIncomingMessage msg)
        {
            return new Vector2f(msg.ReadFloat(), msg.ReadFloat());
        }

        public static void Detach<T>(this LinkedListNode<T> node)
        {
            node.List.Remove(node);
        }

        public static int ValueAsInt(this XmlAttribute attr, int def=0)
        {
            return attr != null ? Convert.ToInt32(attr.Value) : def;
        }

        public static bool ValueAsInt(this XmlAttribute attr, out int val)
        {
            if (attr == null)
            {
                val = 0;
                return false;
            }
            val = Convert.ToInt32(attr.Value);
            return true;
        }

        public static float ValueAsFloat(this XmlAttribute attr, float def=0)
        {
            return attr != null ? Convert.ToSingle(attr.Value) : def;
        }

        public static bool ValueAsFloat(this XmlAttribute attr, out float val)
        {
            if (attr == null)
            {
                val = 0;
                return false;
            }
            val = Convert.ToSingle(attr.Value);
            return true;
        }

        public static bool ToBool(this XmlAttribute attr, bool def = false)
        {
            return attr != null ? Convert.ToBoolean(attr.Value) : def;
        }

        public static bool ValueAsFloat(this XmlAttribute attr, out bool val)
        {
            if (attr == null)
            {
                val = false;
                return false;
            }
            val = Convert.ToBoolean(attr.Value);
            return true;
        }

        public static string ValueAsString(this XmlAttribute attr, string def=null)
        {
            return attr != null ? attr.Value : def;
        }

        public static bool ValueAsFloat(this XmlAttribute attr, out string val)
        {
            if (attr == null)
            {
                val = null;
                return false;
            }
            val = attr.Value;
            return true;
        }
    }
}
