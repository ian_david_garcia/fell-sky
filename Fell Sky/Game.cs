﻿using System;
using System.Collections.Generic;
using System.IO;
using CSScriptLibrary;
using FellSky.AssetManagement;
using FellSky.Core;
using FellSky.GameStates;
using SFML;
using SFML.Graphics;

namespace FellSky
{
    public enum GameMode
    {
        None=0, Server=1, Client=2
    }

    public static class Game
    {
        public const uint Fps = 30;
        public static TimeSpan TimeStep = TimeSpan.FromMilliseconds(1d / Fps);

        private static readonly Stack<IGameState> GameStates = new Stack<IGameState>();
        private static bool _quit;

        //public static readonly TimeSpan TimePerFrame = TimeSpan.FromSeconds(1d/FPS);
        public static string DataDirectory = "../Data/";
        public static string PreloadedAssetsFile = "PreloadedAssets.xml";
        public static string MainAssetsFile = "Assets.xml";
        public static string MainScriptFile = "Scripts/Init.cs";
        public static string ScriptInitMethod = "FellSky.Script.Initialize";

        public static string Version = "Client Revision 8 Version 0.0.1";

        public static Player CurrentPlayer;

        public static Universe CurrentUniverse;
        public static World CurrentWorld;
        public static AsmHelper Scripting;


        public static IGameState CurrentState
        {
            get { return GameStates.Count > 0 ? GameStates.Peek() : null; }
        }

        public static RenderWindow RenderWindow { get; private set; }

        public static event Action OnQuit;

        public static void PushGameState(IGameState state)
        {
            var prevState = CurrentState;
            if (prevState != null) prevState.Deactivate(state);
            GameStates.Push(state);
            state.Initialize();
            state.Activate(prevState);
        }

        public static IGameState PopGameState()
        {
            if (GameStates.Count <= 0) return null;
            var popState = GameStates.Pop();
            popState.Deactivate(CurrentState);
            if (CurrentState != null) CurrentState.Activate(popState);
            return popState;
        }

        public static void Quit()
        {
            _quit = true;
        }

        private static void Initialize()
        {
            InitializeGraphics();
            Directory.SetCurrentDirectory(DataDirectory);
            InitializeScripting();
            UI.Core.Initialize();
            
            AssetManager.RegisterAssetTypes();
            AssetManager.LoadFromFile(PreloadedAssetsFile);

            PushGameState(new AssetLoadState());

            CurrentUniverse = Universe.LoadDefault();

            RenderWindow.OnClose += (o, e) => Quit();
        }

        private static void InitializeScripting()
        {
            try
            {
                Scripting = new AsmHelper(CSScript.Load(MainScriptFile));
                Scripting.Invoke(ScriptInitMethod);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        private static void InitializeGraphics()
        {
            RenderWindow = new RenderWindow(new Window.VideoMode(1200,700),"Fell Sky", (uint) Window.Styles.Close );
            RenderWindow.SetFrameLimit(Game.Fps);
            IncludeAsset.LowGraphicsMemMode = Texture.MaxTextureSize < 2048;
            View view = RenderWindow.DefaultView;
            view.Center = new Vector2f(0, 0);
            RenderWindow.View = view;
            RenderWindow.Clear();
            RenderWindow.Display();      
        }

        private static int Main(string[] args)
        {
            Start();
            return 0;
        }

        private static void Run()
        {
            while (!_quit)
            {
                //Network.Core.Client.Update(ClientTimeStep);
                RenderWindow.DispatchEvents();
                if (GameStates.Count > 0)
                {
                    CurrentState.Update(TimeStep);
                    CurrentState.Render(TimeStep, RenderWindow);
                    RenderWindow.Display();                    
                } else Quit();
            }
            if (OnQuit != null) OnQuit();
        }


        private static void Shutdown()
        {
            RenderWindow.Close();
        }


        public static void Start()
        {

            Initialize();
            Run();
            Shutdown();

        }
    }
}
