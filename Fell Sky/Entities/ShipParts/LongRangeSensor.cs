﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FellSky.Core;

namespace FellSky.Entities.ShipParts
{
    public class LongRangeSensor: ShipPart
    {
        public HashSet<ISensorContact> SensorContacts;
        public event Action<Entity, ISensorContact> OnContactDetect, OnContactLost;


        public override void Spawn(World world)
        {
            SensorContacts=new HashSet<ISensorContact>();
            base.Spawn(world);
            foreach (var obj in world.Entities.OfType<ISensorContact>())
                AddSensorContact(obj);

            world.OnAddEntity += AddWorldContact;
        }

        public override void Dispose()
        {
            base.Dispose();
            World.OnAddEntity -= AddWorldContact;
        }

        private void AddWorldContact(Entity obj)
        {
            var e = obj as ISensorContact;
            if (e != null) AddSensorContact(e);
        }

        public void AddSensorContact(ISensorContact contact)
        {
            SensorContacts.Add(contact);
            contact.OnDispose += DisposeContact;
            OnContactDetect(this, contact);
            var child = contact.GetChildSensorContacts();
            if (child == null) return;
            
            foreach (var c in child)
                AddSensorContact(c);
            
        }

        public void RemoveSensorContact(ISensorContact sObj)
        {
            SensorContacts.Remove(sObj);
            OnContactLost(this, sObj);
        }

        private void DisposeContact(Entity obj)
        {
            var sObj = (ISensorContact)obj;
            obj.OnDispose -= DisposeContact;
            if (sObj != null) RemoveSensorContact(sObj);
        }
    }
}
