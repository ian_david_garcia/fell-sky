﻿using System;
using FellSky.Combat;

namespace FellSky.Entities.ShipParts
{
    /// <summary>
    /// Hull Shields are energy fields that coat/permeate the armored hull of a ship.
    /// </summary>

    public class HullShield: ShipPart, IShield
    {
        public float Capacity { get; set; }
        public float Entropy { get; set; }
        public float EntropyDispelRate { get; set; }
        public event Action<IShield, Damage> BeforeShieldDamage;
        public event Action<IShield, Damage> AfterShieldDamage;
        public event Action<IShield, Damage> OnShieldOverload;
        public bool IsOverloaded { get; private set; }
    }
}
