﻿using SFML;

namespace FellSky.Entities.ShipParts
{
    public class DockingPort
    {
        public float DockingRadius { get; set; }
        public Vector2f Position { get; set; }
        public int DockingSlots { get; set; }
    }

}
