﻿using System;
using System.Collections.Generic;
using FellSky.Combat;
using FellSky.Core;
using FellSky.Graphics;
using SFML;
using SFML.Graphics;

namespace FellSky.Entities.ShipParts
{
    [Serializable]
    public abstract class ShipPart: SpatialEntity, IDamageable, ICollidable, ISensorContact, IChildEntity<Ship>, ISprite, IRenderable, ITargetable
    {
        public enum RenderModes
        {
            Normal = 0,
            Health = 1,
            Highlight = 2,
            Blueprint = 3,
            Stealth = 4,
        }

        public string SpriteID { get; set; }
        public Sprite Sprite { get; set; }
        public Color Color { get; set; }

        public float CurrentHealth { get; set; }
        public float MaxHealth { get; set; }

        public Ship Parent { get; protected set; }

        public Vector2f GlobalPosition { get; set; }
        public Vector2f GlobalRotation { get; set; }
        public Vector2f GlobalScale { get; set; }

        public Vector2f LinearVelocity { get; set; }
        public float AngularVelocity { get; set; }
        public long EmittedSignatures { get; set; }

        public virtual bool CanCollideWith(ICollidable obj)
        {
            var collide = obj;
            if (collide == null) return false;
            return collide is ShipPart;
        }
        
        public IEnumerable<ISensorContact> GetChildSensorContacts()
        {
            return null;
        }

        public virtual void Spawn(World world, Ship ship)
        {
            Parent = ship;
            base.Spawn(world);
        }

        public event Action<IDamageable, Damage> BeforeDamage;
        public event Action<IDamageable, Damage> AfterDamage;


        public void ApplyDamage(Damage damage)
        {
            if (BeforeDamage != null) BeforeDamage(this,damage);
            damage.ApplyTo(this);
            if (AfterDamage != null) AfterDamage(this, damage);
        }

        public LinkedListNode<IRenderable> RenderNode { get; set; }
        public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnBeforeRender;
        public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnAfterRender;


        public void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states, int renderMode = 0)
        {
            Parent.SpriteBatch.Draw(
                Sprite.Texture,
                Position,
                Sprite.TextureRect,
                Color,
                Scale,Origin+Sprite.Origin,
                Rotation,
                states.BlendMode
                );
        }

        public bool IsFriendlyTo(IFriendOrFoe obj)
        {
            return Parent.IsFriendlyTo(obj);
        }
    }
}
