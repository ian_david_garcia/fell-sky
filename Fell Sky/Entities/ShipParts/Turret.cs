﻿using System;
using System.ComponentModel;

using FellSky.Core;
using SFML;

namespace FellSky.Entities.ShipParts
{
    [Serializable]
    public class Turret: ShipPart
    {
        public struct WeaponSlot
        {
            [TypeConverter(typeof(Archetypes<Weapon>.StandardValueConverter))]
            public string WeaponID { get; set; }
            public Vector2f Position { get; set; }
            public float Rotation { get; set; }
        }

        public WeaponSlot[] Slots { get; set; }
        public TimeSpan StaggeredFireDelay { get; set; }
        public Weapon[] Weapons;

        [Category("Turret")]
        public float TrackingArc { get; set; }
        [Category("Turret")]
        public float TrackSpeed { get; set; }

        public Vector2f TrackedPosition;
        public float TurretRotation=0;

        public void Fire()
        {
            
        }
    }
}
