﻿using System;
using FarseerPhysics.Dynamics;
using FellSky.Combat;
using FellSky.Core;
using World = FellSky.Core.World;

namespace FellSky.Entities.ShipParts
{
    public class AreaShield: ShipPart, IShield
    {
        public float Radius { get; set; }

        public float Capacity { get; set; }
        public float Entropy { get; set; }
        public float EntropyDispelRate { get; set; }
        public event Action<IShield, Damage> BeforeShieldDamage;
        public event Action<IShield, Damage> AfterShieldDamage;
        public event Action<IShield, Damage> OnShieldOverload;

        public bool IsOverloaded { get { return Entropy > Capacity; } }

        [NonSerialized] public AreaField Field;

        public class AreaField: ICollidable, IDamageable
        {
            public readonly AreaShield Shield;

            public AreaField(AreaShield shield)
            {
                Shield = shield;
            }

            public bool CanCollideWith(ICollidable obj)
            {
                return obj is Projectiles.Projectile;
            }

            public float CurrentHealth
            {
                get { return Shield.Capacity - Shield.Entropy; }
                set { Shield.Entropy = Shield.Capacity - value; }
            }
            public float MaxHealth {
                get { return Shield.Capacity; }
                set { Shield.Capacity = value; }
            }

            public event Action<IDamageable, Damage> BeforeDamage;
            public event Action<IDamageable, Damage> AfterDamage;

            public void ApplyDamage(Damage damage)
            {
                if (BeforeDamage != null) BeforeDamage(this, damage);
                CurrentHealth -= damage.Amount;
                if (AfterDamage != null) AfterDamage(this, damage);
            }
        }

        public override void Spawn(World world, Ship ship)
        {
            base.Spawn(world, ship);
            Field=new AreaField(this);
        }
    }
}
