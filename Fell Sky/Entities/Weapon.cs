﻿using System;
using System.ComponentModel;

using FellSky.Core;
using FellSky.Entities.Projectiles;
using FellSky.Inventory;
using SFML;
using SFML.Graphics;

namespace FellSky.Entities
{
    [Serializable]
    public class Weapon: Entity, IItemType, IChildEntity<Entity>
    {

        public string ArchetypeID { get; set; }



        public string ItemName { get; set; }
        public string ItemCategory { get; set; }
        public float Mass { get; set; }
        public float Volume { get; set; }

        [Category("Projectile")]
        public Projectile Projectile { get; set; }
        [Category("Projectile")]
        public int ProjectilesPerShot { get; set; }
        [Category("Weapon"), Description("Delay between shots, in seconds")]
        public float FireDelay { get; set; }
        [Category("Weapon"), Description("Delay between reloads, in seconds. Added on top of fire cooldown.")]
        public float ReloadDelay { get; set; }
        [Category("Weapon")]
        public float ShotSpread { get; set; }
        [Category("Ammo")]
        public int CurrentMagazineAmmoLevel { get; set; }
        [Category("Ammo")]
        public int MagazineSize { get; set; }
        [Category("Ammo")]
        public int MaxAmmo { get; set; }
        [Category("Ammo")]
        public int AmmoUse { get; set; }
        [Category("Ammo")]
        public int CurrentAmmoLevel { get; set; }
        [Category("Power")]
        public int PowerUsePerShot { get; set; }
        [Category("Power")]
        public int PowerUseIdle { get; set; }
        [Category("Power")]
        public float EnergyUseInactive { get; set; }
        [Category("Power")]
        public float EnergyUseActive { get; set; }
        [Category("Graphics")]
        public bool IsFiredBeneathHull { get; set; }

        protected override void CloneObject(ref object clone)
        {
            var obj = (Weapon) clone;
            
            obj.Projectile = (Projectile)Projectile.Clone();
            
            base.CloneObject(ref clone);
        }

        public virtual void Fire()
        {
            var projectile = Projectile.Clone();
        }

        public Entity Parent { get; set; }
        public Vector2f GlobalPosition { get; set; }
        public Vector2f GlobalRotation { get; set; }
        public Vector2f GlobalScale { get; set; }
    }
}
