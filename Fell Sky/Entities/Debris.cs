﻿using FellSky.Core;
using SFML.Graphics;

namespace FellSky.Entities
{
    public class Debris: SpatialEntity, ISprite
    {
        public string SpriteID { get; set; }
        public Sprite Sprite { get; set; }
    }
}
