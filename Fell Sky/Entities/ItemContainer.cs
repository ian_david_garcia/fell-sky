﻿using System;
using System.Collections.Generic;
using FellSky.Core;
using FellSky.Inventory;
using SFML.Graphics;

namespace FellSky.Entities
{
    [Serializable]
    public class ItemContainer: SpatialEntity, ISprite
    {
        public List<ItemStack> Items { get; set; }

        public string SpriteID { get; set; }
        public Sprite Sprite { get; set; }
    }
}
