﻿using System;
using System.Collections.Generic;
using FellSky.Combat;
using FellSky.Graphics;
using FellSky.Core;
using FellSky.Entities.ShipParts;
using FellSky.Utilities;
using SFML;
using SFML.Graphics;

namespace FellSky.Entities
{
    /// <summary>
    /// Base ship type, common to both server and client ships.
    /// </summary>
    public class Ship : SpatialEntity, ISensorContact, IArchetype, IRenderable, IFriendOrFoe
    {
        [NonSerialized]
        private SpriteBatch _spriteBatch;

        public string GivenName { get; set; }

        public List<ShipPart> Parts { get; set; }

        public string ArchetypeID { get; set; }
        
        public Vector2f LinearVelocity { get; set; }
        public float AngularVelocity { get; set; }

        public long EmittedSignatures { get; set; }

        
        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
            private set { _spriteBatch = value; }
        }

        public IEnumerable<ISensorContact> GetChildSensorContacts()
        {
            return Parts;
        }

        public override void Spawn(World world)
        {
            SpriteBatch=new SpriteBatch();
            world.AddRenderable(this);
            foreach(var part in Parts)
                part.Spawn(world, this);
            base.Spawn(world);
        }

        public override void Dispose()
        {
            if(RenderNode!=null) RenderNode.Detach();
            foreach(var part in Parts)
                part.Dispose();
            base.Dispose();
        }

        public void AddPart(ShipPart part)
        {
            if (part.Parent != null) throw new ArgumentException("Cannot add part - already added to another ship.");
            Parts.Add(part);
            if (Status != EntityStatus.Spawned) return;
            part.Spawn(World,this);
            part.WorldNode = WorldNode;
        }

        public void RemovePart(ShipPart part)
        {
            if(part.Parent!=this) throw new ArgumentException("Cannot remove part - not a member of ship.");
            Parts.Remove(part);
            if (Status != EntityStatus.Spawned) return;
            part.Dispose();
        }

        public override void Update(TimeSpan timeStep)
        {

            foreach (var shipPart in Parts)
                shipPart.Update(timeStep);          

            base.Update(timeStep);
        }

        public LinkedListNode<IRenderable> RenderNode { get; set; }
        public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnBeforeRender;
        public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnAfterRender;

        public void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states, int renderMode = 0)
        {
            if (OnBeforeRender != null) OnBeforeRender(this, timeSpan, target, camera, states, renderMode);
            SpriteBatch.Begin();
            foreach (var part in Parts)
            {
                part.Render(timeSpan,target,camera,states,renderMode);
            }
            SpriteBatch.End();
            if (OnAfterRender != null) OnAfterRender(this, timeSpan, target, camera, states, renderMode);
        }

        public bool IsFriendlyTo(IFriendOrFoe obj)
        {
            return false;
        }
    }
}
