﻿using System;
using System.Collections.Generic;
using System.Xml;
using FellSky.AssetManagement;
using FellSky.Core;
using FellSky.Graphics;
using SFML;
using SFML.Graphics;

namespace FellSky.Entities
{
    public class Backdrop: Entity, IRenderable
    {
        public string TextureID { get; set; }
        public static Dictionary<string, string> Backdrops = new Dictionary<string, string>();

        public static void LoadBackdrops(string xmlID)
        {
            var xml = XmlAsset.Get(xmlID);
            foreach (XmlNode node in xml.SelectNodes("Backdrop"))
            {
                Backdrops[node.Attributes["id"].Value] = node.Attributes["filename"].Value;
            }
            Console.WriteLine("[FellSky.Entities.Backdrop] Loaded {0} backdrop definitions.",Backdrops.Count);
        }

        public Sprite Sprite;
        public Texture Texture;

        public Backdrop(string texID)
        {
            TextureID = texID;
        }

        public override void Spawn(World world)
        {

            base.Spawn(world);
            Texture = new Texture(Backdrops[TextureID]);
            Sprite = new Sprite(Texture);
            world.AddRenderable(this, RenderPosition.First);
            Sprite.Origin = new Vector2f(Texture.Size.X / 2f, Texture.Size.Y / 2f);
            //Scale = new Vector2f(5.5f,5.5f);
            Sprite.Color = Color.White;
            float s = Game.RenderWindow.Size.X / (float)Texture.Size.X;
            Sprite.Scale = new Vector2f(s, s);

        }

        public override void Dispose()
        {
            World.RemoveRenderable(this);
            base.Dispose();
            Sprite.Texture = null;
            Sprite = null;
            Texture.Dispose();
            Texture = null;
        }

        public LinkedListNode<IRenderable> RenderNode { get; set; }
        public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnBeforeRender;
        public event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnAfterRender;
        public void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states, int renderMode = 0)
        {
            if (OnBeforeRender != null) OnBeforeRender(this, timeSpan, target, camera, states, renderMode);
            Sprite.Draw(target, states);
            if (OnAfterRender != null) OnBeforeRender(this, timeSpan, target, camera, states, renderMode);
        }
    }
}
