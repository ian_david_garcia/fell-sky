﻿using System;
using System.Collections.Generic;
using FellSky.Core;
using FellSky.EntityComponents;

namespace FellSky.Entities
{
    public enum EntityStatus
    {
        Invalid, Spawned, Disposed
    }

    [Serializable]
    public class Entity: IUpdateable, ITagged, IDisposable, ICloneable
    {
        [NonSerialized]
        internal LinkedListNode<Entity> WorldNode;

        public Entity()
        {
            Status = EntityStatus.Invalid;
        }

        public World World
        {
            get { return _world; }
            private set { _world = value; }
        }

        public int ID { get; set; }

        public EntityStatus Status
        {
            get { return _status; }
            private set { _status = value; }
        }

        public string[] Tags { get; set; }

        [NonSerialized]
        public List<IEntityComponent> Components=new List<IEntityComponent>();
        [NonSerialized]
        private World _world;
        [NonSerialized]
        private EntityStatus _status = EntityStatus.Invalid;

        public event Action<Entity, World> OnSpawn;
        public event Action<Entity> OnDispose;
        public event Action<TimeSpan> OnUpdate;

        public virtual void Spawn(World world)
        {
            ID = GetHashCode();
            if(Status==EntityStatus.Spawned) throw new InvalidOperationException("Cannot spawn: object already spawned.");
            Status=EntityStatus.Spawned;
            World = world;
            foreach (var component in Components)
                component.Spawn(this);
            if (OnSpawn != null) OnSpawn(this,world);
        }

        public virtual void Dispose()
        {
            if (Status != EntityStatus.Spawned) throw new InvalidOperationException("Cannot dispose: object not spawned.");
            Status = EntityStatus.Disposed;
            if (OnDispose != null) OnDispose(this);
            foreach (var component in Components)
                component.Dispose();
            World = null;
        }

        public object Clone()
        {
            var e = MemberwiseClone();
            CloneObject(ref e);
            return e;
        }

        protected virtual void CloneObject(ref object clone)
        {
            World = null;
            WorldNode = null;
            Tags = (string[])Tags.Clone();
            Status=EntityStatus.Invalid;
        }

        public virtual void Update(TimeSpan timeStep)
        {
            foreach (var component in Components)
                component.Update(timeStep);
            if (OnUpdate != null) OnUpdate(timeStep);
        }

        

    }
}
