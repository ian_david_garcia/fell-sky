﻿using System;
using FellSky.Combat;
using FellSky.Core;

namespace FellSky.Entities.Projectiles
{
    public class Bullet: Projectile, ICollidable
    {

        public string SpriteID { get; set; }
        /// <summary>
        /// Speed of the prjectile, in meters/second
        /// </summary>
        public float Speed { get; set; }

        public TimeSpan Lifetime { get; set; }

        public override float MinRange
        {
            get { return 0; }
        }

        public override float MaxRange
        {
            get { return Lifetime.Seconds * Speed; }
        }

        public bool CanCollideWith(ICollidable obj)
        {
            return obj is ITargetable && !IsFriendlyTo((IFriendOrFoe) obj);
        }
    }
}
