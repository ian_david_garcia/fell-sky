﻿using FellSky.Combat;

namespace FellSky.Entities.Projectiles
{
    public class Beam: Projectile
    {

        /// <summary>
        /// Beam length, in meters.
        /// </summary>
        public float Length { get; set; }

        public override float MinRange
        {
            get { return 0; }
        }

        public override float MaxRange
        {
            get { return Length; }
        }


    }
}
