﻿
using FellSky.Combat;
using FellSky.Core;
using SFML;

namespace FellSky.Entities.Projectiles
{
    public abstract class Projectile: SpatialEntity, IMoveable, IDamageSource, IFriendOrFoe, IArchetype
    {
        public string ArchetypeID { get; set; }

        public Damage Damage { get; set; }
        public bool IsIndiscriminate { get; set; }

        public virtual float MinRange { get; protected set; }
        public virtual float MaxRange { get; protected set; }
        

        public Vector2f LinearVelocity { get; set; }
        public float AngularVelocity { get; set; }

        public IFriendOrFoe Owner { get; private set; }
        public Weapon Weapon { get; private set; }

        protected Projectile()
        {
            IsIndiscriminate = false;
        }

        public virtual void FireFrom(IFriendOrFoe owner, Weapon weapon)
        {
            Owner = owner;
            Weapon = weapon;
            Damage = new Damage();
        }

        public virtual bool IsFriendlyTo(IFriendOrFoe obj)
        {
            return !IsIndiscriminate && Owner.IsFriendlyTo(obj);
        }

        
    }
}
