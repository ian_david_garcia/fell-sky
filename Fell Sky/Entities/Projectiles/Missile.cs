﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FellSky.Combat;
using FellSky.Core;

namespace FellSky.Entities.Projectiles
{
    public class Missile: Projectile, ICollidable
    {
        public float Fuel { get; set; }
        public float FuelUse { get; set; }
        public float Thrust { get; set; }

        public bool CanCollideWith(ICollidable obj)
        {
            return obj is ITargetable && !IsFriendlyTo((IFriendOrFoe)obj);
        }
    }
}
