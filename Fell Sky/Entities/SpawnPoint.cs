﻿using System;
using System.Collections.Generic;
using FellSky.Core;

namespace FellSky.Entities
{
    public class SpawnPoint: SpatialEntity
    {
        public class SpawnItem
        {
            public string IncludeTags, ForbidTags;
            public bool IgnoreSpawnTimer = false;
            public float Chance;
            public TimeSpan SpawnDelay;
            public List<SpatialEntity> Entities = new List<SpatialEntity>();
        }

        public float Radius { get; set; }
        public float InboundArcAngle { get; set; }
        public float InboundArcLength { get; set; }
        public TimeSpan SpawnTimer { get; set; }

        
        
    }
}
