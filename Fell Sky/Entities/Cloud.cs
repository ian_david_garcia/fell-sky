﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FellSky.Core;
using SFML.Graphics;

namespace FellSky.Entities
{
    public class Cloud: SpatialEntity, ISprite
    {
        public string SpriteID { get; set; }
        public Sprite Sprite { get; set; }
    }
}
