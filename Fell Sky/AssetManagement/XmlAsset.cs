﻿using System.Xml;

namespace FellSky.AssetManagement
{
    public class XmlAsset : Asset<XmlNode>
    {
        public string TagName;
        public XmlAsset(XmlNode xml)
            : base(xml)
        {
            TagName = xml.Name;
            Value = xml;
        }
    }

}
