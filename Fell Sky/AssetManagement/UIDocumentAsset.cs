﻿using System.Xml;
using LibRocket;

namespace FellSky.AssetManagement
{
    public class UIDocumentAsset: Asset<Document>
    {
        public string Filename;

        public UIDocumentAsset(XmlNode xml) : base(xml)
        {
            Filename = AssetManager.CurrentPath + xml.Attributes["filename"].Value;
            Value = LibRocket.Core.DefaultContext.LoadDocument(Filename);
        }
    }
}
