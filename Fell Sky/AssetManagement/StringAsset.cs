﻿using System;
using System.Xml;

namespace FellSky.AssetManagement
{
    public class StringAsset : Asset<String>
    {
        public string Language;
        public StringAsset(XmlNode xml)
            : base(xml)
        {
            var node = xml.Attributes["lang"];
            Language = node != null ? node.Value : "en";
            Value = xml.InnerText;
        }
    }
}
