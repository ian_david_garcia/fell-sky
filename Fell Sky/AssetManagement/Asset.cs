﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml;

namespace FellSky.AssetManagement
{
    public abstract class Asset<T>
    {
        public string ID;
        public T Value;

        protected Asset(XmlNode xml)
        {
            if (xml == null) return;
            ID = xml.Attributes["id"].Value;
            Assets[ID] = this;
        }

        public static T Get(string id)
        {
            return Assets[id].Value;
        }

        public override string ToString()
        {
            return ID;
        }

        public static int Count {get { return Assets.Count; }}

        public static Dictionary<string, Asset<T>> Assets = new Dictionary<string, Asset<T>>();

        public class StandardValueConverter : TypeConverter
        {
            public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                return new StandardValuesCollection(Assets.Keys.ToArray());
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }
        }
    }
}
