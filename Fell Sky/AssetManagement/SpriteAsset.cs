﻿using System.Collections.Generic;
using System.Xml;
using FellSky.Utilities;
using SFML;
using SFML.Graphics;

namespace FellSky.AssetManagement
{
    public class SpriteAsset : Asset<Sprite>
    {
        public string TextureID;
        public string Type, Subtype;
        public Dictionary<string, Sprite> AltSprites = new Dictionary<string, Sprite>();

        public SpriteAsset(XmlNode xml)
            : base(xml)
        {
            Value=new Sprite
            {
                Texture = Asset<Texture>.Assets[xml.Attributes["texture"].Value].Value,
                TextureRect = new IntRect(
                    xml.Attributes["x"].ValueAsInt(),
                    xml.Attributes["y"].ValueAsInt(),
                    xml.Attributes["w"].ValueAsInt(),
                    xml.Attributes["h"].ValueAsInt()
                    )
            };

            Value.Origin = new Vector2f(
                xml.Attributes["origin_x"].ValueAsInt(Value.TextureRect.Width / 2),
                xml.Attributes["origin_y"].ValueAsInt(Value.TextureRect.Height / 2)
                );
            TextureID = xml.Attributes["texture"].Value;

            XmlAttribute attr = xml.Attributes["type"];
            Type = attr != null ? attr.Value : null;

            attr = xml.Attributes["subtype"];
            Subtype = attr != null ? attr.Value : null;

            for (int i = 0; i < xml.ChildNodes.Count; i++)
            {
                var node = xml.ChildNodes[i];
                if (node.Name == "AltSprite")
                {
                    var altSprite = new Sprite(Value);
                    attr = xml.Attributes["texture"];
                    altSprite.Texture = attr!=null ? Asset<Texture>.Assets[attr.ValueAsString()].Value : Value.Texture;

                    var rect = new IntRect();

                    bool isPosRelative = node.Attributes["relativepos"].ToBool();
                    bool isSizeRelative = node.Attributes["relativesize"].ToBool();
                    
                    rect.Left = node.Attributes["x"].ValueAsInt() + (isPosRelative ? Value.TextureRect.Left : 0);
                    rect.Top  = node.Attributes["y"].ValueAsInt() + (isPosRelative ? Value.TextureRect.Top : 0);

                    rect.Width  = node.Attributes["w"].ValueAsInt() + (isSizeRelative ? Value.TextureRect.Width : 0);
                    rect.Height = node.Attributes["h"].ValueAsInt() + (isSizeRelative ? Value.TextureRect.Height : 0);

                    altSprite.TextureRect = rect;

                    AltSprites[node.Attributes["id"].ValueAsString()] = altSprite;
                    
                }
            }
        }
    }


}
