﻿using System.Xml;
using SFML.Graphics;

namespace FellSky.AssetManagement
{
    public class TextureAsset : Asset<Texture>
    {
        public string Filename;

        public TextureAsset(string filename): base(null)
        {
            Filename = filename;
            Value = new Texture(Filename) {Smooth = true, Repeated = true};
        }

        public TextureAsset(XmlNode xml)
            : base(xml)
        {
            Filename = AssetManager.CurrentPath + xml.Attributes["filename"].Value;
            Value = new Texture(Filename);
        }
    }
}
