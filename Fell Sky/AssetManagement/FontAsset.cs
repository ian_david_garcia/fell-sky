﻿using System;
using System.Xml;
using SFML.Graphics;

namespace FellSky.AssetManagement
{
    public class FontAsset : Asset<Font>
    {
        public static Font MainFont;
        public string Filename;

        public FontAsset(XmlNode xml) : base(xml)
        {
            Filename = AssetManager.CurrentPath + xml.Attributes["filename"].Value;
            Value = new Font(Filename);
            if (xml.Attributes["mainfont"] != null && Convert.ToBoolean(xml.Attributes["mainfont"].Value)) MainFont = Value;
            LibRocket.Core.LoadFont(Filename);
        }
    }
}
