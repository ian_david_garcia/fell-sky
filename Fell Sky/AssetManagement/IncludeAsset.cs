﻿using System;
using System.Xml;

namespace FellSky.AssetManagement
{
    public class IncludeAsset : Asset<string>
    {
        public static bool LowGraphicsMemMode = false;
        public uint MinTexSize, MaxTexSize;

        public IncludeAsset(XmlNode xml)
            : base(xml)
        {
            Value = xml.Attributes["filename"].Value;
            if (xml.Attributes["lowgraphics"] != null)
            {
                if(Convert.ToBoolean(xml.Attributes["lowgraphics"].Value) == LowGraphicsMemMode) AssetManager.LoadFromFile(Value);
            }
            else AssetManager.LoadFromFile(Value);
        }
    }
}
