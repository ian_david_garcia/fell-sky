﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace FellSky.AssetManagement
{
    public static class AssetManager
    {
        public static string CurrentPath = "";
        private static readonly Dictionary<string, Type> AssetLoaders = new Dictionary<string, Type>();

        public static void Register(string resID, Type type)
        {
            AssetLoaders[resID] = type;
        }

        public static void LoadFromFile(string filename)
        {
            var document = new XmlDocument();
            document.Load(filename);
            var lastPath = CurrentPath;
            CurrentPath = Path.GetDirectoryName(filename);

            foreach (XmlNode node in document.GetElementsByTagName("Assets")[0])
            {
                Activator.CreateInstance(
                    AssetLoaders.ContainsKey(node.Name) ? AssetLoaders[node.Name] : typeof(XmlAsset), node);
            }
            CurrentPath = lastPath;
        }

        public static void RegisterAssetTypes()
        {
            Register("Include",typeof(IncludeAsset));
            Register("String",typeof(StringAsset));
            Register("Xml",typeof(XmlAsset));
            Register("Sprite", typeof(SpriteAsset));
            Register("Texture", typeof(TextureAsset));
            Register("UIDocument", typeof(UIDocumentAsset));
            Register("Font", typeof(FontAsset));
        }

        public static void PrintAssetCounts()
        {
            foreach (var t in AssetLoaders)
            {
                var field = t.Value.BaseType.GetProperty("Count",BindingFlags.Static | BindingFlags.Public);
                Console.WriteLine("[Game.Initialize] {0} has {1} assets loaded.", t.Value.Name,
                    field.GetValue(t.Value.BaseType));

            }

        }
    }

}
