﻿using System;
using FellSky.AssetManagement;
using FellSky.Client.Graphics;
using FellSky.Core;
using FellSky.Entities;
using FellSky.Utilities;
using SFML.Graphics;

namespace FellSky.Client.EntityComponents
{
    public class SpriteComponent: RenderComponent
    {
        public ISpriteID SpriteEntity;

        public Sprite Sprite;
      
        public override void Spawn(Entity entity)
        {
            base.Spawn(entity);
            SpriteEntity = entity as ISpriteID;
            if (SpriteEntity == null)
            {
                this.Log("Error: Entity does not implement ISpriteID.");
                throw new ArgumentException("Entity does not implement ISpriteID.");
            }
            Sprite = Asset<Sprite>.Get(SpriteEntity.SpriteID);
        }

        public override void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states)
        {
            Sprite.Draw(target, states);
            base.Render(timeSpan, target, camera, states);

        }
    }
}
