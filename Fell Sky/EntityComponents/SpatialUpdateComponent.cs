﻿using System;
using FellSky.Core;
using FellSky.Entities;
using FellSky.EntityComponents;
using FellSky.Network;
using FellSky.Network.Messages;
using FellSky.Utilities;
using Lidgren.Network;

namespace FellSky.Client.EntityComponents
{
    public class SpatialUpdateComponent<T>: IEntityComponent, INetworkListener
        where T: Entity, ISpatial, IMoveable
    {
        private ulong _lastReceivedFrame;

        private readonly SpatialUpdateMessage _msg=new SpatialUpdateMessage();
        private bool _updateEntity;

        public bool InterpolationEnabled = true;

        public event Action<TimeSpan> OnUpdate;

        public void Update(TimeSpan timeStep)
        {
            if (OnUpdate != null) OnUpdate(timeStep);
            if (_updateEntity)
            {
                Entity.Position = _msg.Position;
                Entity.Rotation = _msg.Rotation;
                Entity.Origin = _msg.Origin;
                Entity.Scale = _msg.Scale;
                Entity.LinearVelocity = _msg.LinearVelocity;
                Entity.AngularVelocity = _msg.AngularVelocity;
                _updateEntity = false;
            }
            else if (InterpolationEnabled)
            {
                Entity.Position += Entity.LinearVelocity * timeStep.Seconds;
                Entity.Rotation += Entity.AngularVelocity * timeStep.Seconds;
            }
        }

        public void Dispose()
        {
            Network.Core.Client.UnregisterHandler(MessageCode.SpatialUpdate, Entity.ID, this);
        }

        public T Entity { get; private set; }

        public void Spawn(Entity entity)
        {
            _lastReceivedFrame = 0;
            _updateEntity = false;

            Entity = entity as T;
            if (Entity == null)
            {
                const string msg = "Entity is not a {0}.";
                this.Log(msg, typeof(T).ToString());
                throw new ArgumentException(msg, typeof(T).ToString());
            }

            Network.Core.Client.RegisterHandler(MessageCode.SpatialUpdate, Entity.ID, this);

            _msg.Position = Entity.Position;
            _msg.Rotation = Entity.Rotation;
            _msg.Origin = Entity.Origin;
            _msg.Scale = Entity.Scale;
            _msg.LinearVelocity = Entity.LinearVelocity;
            _msg.AngularVelocity = Entity.AngularVelocity;
        }

        public void HandleNetworkMessage(NetMessageHeader header, NetIncomingMessage message)
        {
            if(header.MessageCode!=MessageCode.SpatialUpdate) return;
            if (header.CurrentFrameNum <= _lastReceivedFrame) return;
            _msg.ReadFromMessage(message);
            _lastReceivedFrame=header.CurrentFrameNum;
            _updateEntity = true;
        }
    }
}
