﻿using System;
using System.Collections.Generic;
using FellSky.Client.Graphics;
using FellSky.Entities;
using FellSky.EntityComponents;
using FellSky.Utilities;
using SFML.Graphics;

namespace FellSky.Client.EntityComponents
{
    public class SpriteBatchComponent<T>: IRenderable, IEntityComponent where T : Entity
    {
        public readonly SpriteBatch SpriteBatch = new SpriteBatch();

        public event Action<TimeSpan> OnUpdate;
        public void Update(TimeSpan timeStep)
        {
            if (OnUpdate != null) OnUpdate(timeStep);
        }

        public T Entity { get; private set; }

        public void Spawn(Entity entity)
        {
            Entity = entity as T;
            if (Entity == null)
            {
                const string msg = "Entity is not a {0}.";
                this.Log(msg, typeof(T).ToString());
                throw new ArgumentException(msg, typeof(T).ToString());
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public LinkedListNode<IRenderable> RenderNode { get; set; }
        public event Action<TimeSpan, RenderTarget, RenderStates> OnBeforeRender;
        public event Action<TimeSpan, RenderTarget, RenderStates> OnAfterRender;
        public void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states)
        {
            throw new NotImplementedException();
        }
    }
}
