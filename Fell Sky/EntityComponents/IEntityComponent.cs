﻿using System;
using FellSky.Core;
using FellSky.Entities;

namespace FellSky.EntityComponents
{
    public interface IEntityComponent: IUpdateable, IDisposable, ICloneable
    {
        void Spawn(Entity entity);
    }
}
