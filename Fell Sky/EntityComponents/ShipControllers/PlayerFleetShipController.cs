﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FellSky.Entities;
using FellSky.Network.Messages;
using FellSky.ShipControllers;
using Lidgren.Network;
using SFML;

namespace FellSky.Client.ShipControllers
{
    public class PlayerFleetShipController: IShipController
    {
        private readonly ClientShipControlStatusMessage _controlMsg=new ClientShipControlStatusMessage();

        public PlayerFleetShipController(Ship ship)
        {
            Ship = ship;
        }

        public event Action<TimeSpan> OnUpdate;
        public void Update(TimeSpan timeStep)
        {
            _controlMsg.ThrustForward = Keyboard.IsKeyPressed(UI.Core.Controls.ShipControlUp);
            _controlMsg.ThrustBack = Keyboard.IsKeyPressed(UI.Core.Controls.ShipControlDown);
            _controlMsg.TurnLeft = Keyboard.IsKeyPressed(UI.Core.Controls.ShipControlLeft);
            _controlMsg.TurnRight = Keyboard.IsKeyPressed(UI.Core.Controls.ShipControlRight);
            _controlMsg.ThrustLeft = Keyboard.IsKeyPressed(UI.Core.Controls.ShipControlAltLeft);
            _controlMsg.ThrustRight = Keyboard.IsKeyPressed(UI.Core.Controls.ShipControlAltRight);

            var msg = Network.Core.Client.CreateMessage(_controlMsg);
            Network.Core.Client.SendMessageToServer(msg, NetDeliveryMethod.UnreliableSequenced, 22);
        }

        public Ship Ship { get; private set; }
    }
}
