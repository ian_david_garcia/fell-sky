﻿using System;
using System.Collections.Generic;
using FellSky.Client.Graphics;
using FellSky.Entities;
using FellSky.EntityComponents;
using SFML.Graphics;

namespace FellSky.Client.EntityComponents
{
    public class RenderComponent: IEntityComponent, IRenderable
    {
        public float Parallax;

        public event Action<TimeSpan> OnUpdate;
        public virtual void Update(TimeSpan timeStep)
        {
            if (OnUpdate != null) OnUpdate(timeStep);
        }

        public virtual void Dispose()
        {
            if (RenderNode == null) return;
            RenderNode.List.Remove(RenderNode);
            RenderNode = null;
        }

        public Entity Entity { get; private set; }
        
        public virtual void Spawn(Entity entity)
        {
            Entity = entity;
        }

        public void AddToRenderList(Renderer renderer)
        {
            renderer.AddRenderable(this);
        }

        public LinkedListNode<IRenderable> RenderNode { get; set; }
        public event Action<TimeSpan, RenderTarget, RenderStates> OnBeforeRender;
        public event Action<TimeSpan, RenderTarget, RenderStates> OnAfterRender;

        public virtual void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states)
        {
            if (OnBeforeRender != null) OnBeforeRender(timeSpan, target, states);
            if (OnAfterRender != null) OnAfterRender(timeSpan, target, states);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
