﻿namespace FellSky.Inventory
{
    public class ItemStack
    {
        public IItemType Item { get; set; }
        public int Amount { get; set; }
    }
}
