﻿using System;
using System.ComponentModel;
using FellSky.Core;

namespace FellSky.Inventory
{
    public interface IItemType: IArchetype, ICloneable, ITagged
    {
        [Category("Item")]
        string ItemName { get; set; }
        [Category("Item")]
        string ItemCategory { get; set; }
        [Category("Item")]
        float Mass { get; set; }
        [Category("Item")]
        float Volume { get; set; }

    }
}
