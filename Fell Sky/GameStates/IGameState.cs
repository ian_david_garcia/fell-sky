﻿using System;
using SFML.Graphics;

namespace FellSky.Core
{
    public interface IGameState: IUpdateable, IDisposable
    {
        /// <summary>
        /// Called on first load
        /// </summary>
        void Initialize();
        /// <summary>
        /// Called when activating during state change in
        /// </summary>
        /// <param name="previousState"></param>
        void Activate(IGameState previousState);
        /// <summary>
        /// Called when deactivating during state change out
        /// </summary>
        /// <param name="nextState"></param>
        void Deactivate(IGameState nextState);

        void Render(TimeSpan timeStep, RenderWindow renderWindow);
    }
}
