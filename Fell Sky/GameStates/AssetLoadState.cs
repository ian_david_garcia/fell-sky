﻿using System;
using System.Collections.Generic;
using FellSky.AssetManagement;
using FellSky.Core;
using FellSky.Entities;
using FellSky.Graphics;
using FellSky.Utilities;
using LibRocket;
using SFML.Graphics;

namespace FellSky.GameStates
{
    public class AssetLoadState: IGameState
    {
        public event Action<TimeSpan> OnUpdate;
        public LinkedListNode<IRenderable> RenderNode { get; set; }

        public static event Action<string> OnLoadStateChanged;

        private Document _loadScreen;

        public void Update(TimeSpan timeStep)
        {
            // no updating here, all work is in Initialize()
        }

        public void Initialize()
        {
            _loadScreen = Asset<Document>.Get("loadscreen");
            _loadScreen.Show();
            SetLoadState("Preloading primary assets...");
            AssetManager.LoadFromFile(Game.MainAssetsFile);
            Backdrop.LoadBackdrops("backdrops");
        }

        public void Dispose()
        {
            _loadScreen.Hide();
            SetLoadState("Cleaning up...");
            GC.Collect();
            AssetManager.PrintAssetCounts();
            SetLoadState("Loading Done!");
            
        }

        public void Activate(IGameState previousState)
        {
            Game.PopGameState();
            Dispose();
            Game.PushGameState(new MainMenuState());
        }

        public void Deactivate(IGameState nextState)
        {
            _loadScreen.Hide();
        }

        public void Render(TimeSpan timeStep, RenderWindow renderWindow)
        {
            // no rendering here.
        }

        private void SetLoadState(string str)
        {
            this.Log(str);
            if (OnLoadStateChanged != null) OnLoadStateChanged(str);
            Game.RenderWindow.Clear();
            UI.Core.Update();
            UI.Core.Render();
            Game.RenderWindow.Display();
        }
    }
}
