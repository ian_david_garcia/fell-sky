﻿using System;
using System.Collections.Generic;
using FellSky.AssetManagement;
using FellSky.Core;
using FellSky.Graphics;
using FellSky.Network;
using FellSky.Utilities;
using LibRocket;
using SFML.Graphics;

namespace FellSky.GameStates
{
    public class MainMenuState : IGameState
    {
        public static World World;

        public LinkedListNode<IRenderable> RenderNode { get; set; }
        public event Action<TimeSpan> OnUpdate;

        public Document MainMenuDocument;
        public static Action<ServerData> StartServerHook;

        public void Update(TimeSpan timeStep)
        {
            World.Update(timeStep);
            FellSky.UI.Core.Update();
        }

        public void Render(TimeSpan timeSpan, RenderWindow window)
        {
            window.Clear();
            World.Render(window, timeSpan);
            UI.Core.Render();
        }

        public void Initialize()
        {
            World=new World();
            MainMenuDocument = Asset<Document>.Get("mainmenu");
        }

        public void Dispose()
        {
            World = null;
        }

        public void Activate(IGameState previousState)
        {
            if(MainMenuDocument!=null) MainMenuDocument.Show();
            else this.Log("No main menu document to show!");
        }

        public void Deactivate(IGameState nextState)
        {
            if (MainMenuDocument != null) MainMenuDocument.Hide();
        }
    }
}
