﻿using System;
using System.Collections.Generic;
using FellSky.Core;
using FellSky.Graphics;
using SFML.Graphics;

namespace FellSky.GameStates
{
    public class MainGameState : IGameState
    {
        public event Action<TimeSpan> OnUpdate;
        public void Update(TimeSpan timeStep)
        {
            throw new NotImplementedException();
        }

        public LinkedListNode<IRenderable> RenderNode { get; set; }

        public void Initialize()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Activate(IGameState previousState)
        {
            throw new NotImplementedException();
        }

        public void Deactivate(IGameState nextState)
        {
            throw new NotImplementedException();
        }

        public void Render(TimeSpan timeStep, RenderWindow renderWindow)
        {
            throw new NotImplementedException();
        }
    }
}
