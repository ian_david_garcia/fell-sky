﻿namespace FellSky.Network
{
    /// <summary>
    /// Server to client messages
    /// </summary>
    public enum MessageCode
    {
        KeepAlive = 10,
        Acknowledge,
        Ping,
        InvalidMessageCode,
        InvalidMessageArgument,
        
        ServerData,
        ServerJoinGameAccept,
        ServerRejectConnection,
        ServerDisconnect,
        ServerGameStateChange,
        ServerStartGame,
        ServerEndGame,
        ServerKickPlayer,
        ServerLagWarning,

        ServerStartFrame,
        ServerEndFrame,
        
        SpatialUpdate, // for movable objects: position, rotation, scale, origin, velocity, angular velocity
        ServerEntityMessage,
        ServerSpawnEntityArchetype,
        ServerDestroyEntity,

        ChatSingleMessage,
        ChatAllyMessage,
        ChatBroadcastMessage,

        ClientData,

        RequestArchetypeData,
        ArchetypeData,

        ClientJoinGame,
        ClientCreateGame,
        PlayerReady,
        ClientGameNotReady,
        ClientStartGame,
        ClientQuitGame,
        ClientArchetypeReceived,
        ClientRequestArchetypeList,
        
        ClientShipControlStatus,

        EntitySynchronize,

        SpawnShip,
        SpawnBullet,
        SpawnBeam,
        SpawnBackdrop,
        SpawnCloud,
        SpawnDebris,

        ShipThrustForward,
        ShipThrustBack,
        ShipThrustLeft,
        ShipThrustRight,
        ShipTurnLeft,
        ShipTurnRight,
        ShipDock,
        ShipFire,
        ShipSwitchFireGroup,
        ShipSetFireGroupStatus,
        ShipJettisonItem,
        ShipEquipItem,
        ShipUnequipItem,
        ShipBoardShip,
        ShipInvadeShip,

        Projectile

    }
}
