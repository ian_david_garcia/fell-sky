﻿using Lidgren.Network;

namespace FellSky.Network
{
    public interface INetworkListener
    {
        void HandleNetworkMessage(NetMessageHeader header, NetIncomingMessage message);
    }
}
