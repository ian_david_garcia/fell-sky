﻿using System;
using System.Net;
using FellSky.Core;
using FellSky.Network.Messages;
using FellSky.Utilities;
using Lidgren.Network;

namespace FellSky.Network
{
    public sealed class Client: Peer
    {
        public Player Player {
            get { return Game.CurrentPlayer; }
        }
        public ServerData ServerData { get; private set; }

        public NetClient NetClient {
            get { return (NetClient) NetPeer; }
        }


        public NetConnection ServerConnection;

        public event Action<ServerData> OnServerDetection;
        public event Action<ServerData> OnConnectionApproved;

        public Client(string name="Anonymous") : base(new NetClient(FellSky.Network.Core.ClientConfiguration))
        {
            this.Log("Starting Fell Sky Client...");
            NetClient.Start();
        }

        ~Client()
        {
            if(ServerConnection!=null) ServerConnection.Disconnect("Client shutting down.");
        }

        public void Connect(IPEndPoint endpoint, string username, string password)
        {
            var msg = NetClient.CreateMessage();
            var joinmsg = new TryJoinGameMessage(username,password);
            joinmsg.WriteToMessage(msg);
            NetClient.Connect(endpoint,msg);
        }

        public void Connect(string host, int port, string username, string password)
        {
            var msg = NetClient.CreateMessage();
            var joinmsg = new TryJoinGameMessage(username, password);
            joinmsg.WriteToMessage(msg);
            NetClient.Connect(host, port, msg);
        }


        public void SearchForLocalServers()
        {
            NetClient.DiscoverLocalPeers(FellSky.Network.Core.ServerConfiguration.Port);
            this.Log("Pinging for local servers...");
        }


        protected override void HandleMessage(NetIncomingMessage msg)
        {
            base.HandleMessage(msg);
            switch (msg.MessageType)
            {
                case NetIncomingMessageType.DiscoveryResponse:

                    var data = new ServerData();
                    data.ReadFromMessage(msg);
                    this.Log("Server found: {0}  - {1} / ID: {2}", data.Name, data.Address.ToString(), data.ID.ToString());

                    if (OnServerDetection != null) OnServerDetection(data);
                    break;
                case NetIncomingMessageType.StatusChanged:
                    var status = (NetConnectionStatus) msg.ReadByte();
                    var reason = msg.ReadString();
                    this.Log("Connection Status changed: {0}, Reason: {1}",status,reason);
                    switch (status)
                    {
                        case NetConnectionStatus.Connected:
                            ServerConnection = msg.SenderConnection;
                            var acceptMessage = new ServerJoinGameAcceptMessage(ServerConnection.RemoteHailMessage);
                            ServerData = acceptMessage.ServerData;
                            ServerData.Address = ServerConnection.RemoteEndPoint;
                            Game.CurrentPlayer = acceptMessage.Player;
                            this.Log("Connected to {0} - {1}",ServerData.Name,ServerData.Address.ToString());
                            if (OnConnectionApproved != null) OnConnectionApproved(ServerData);
                            break;
                        case NetConnectionStatus.Disconnecting:
                            ServerConnection = null;
                            break;
                        case NetConnectionStatus.Disconnected:
                            ServerConnection = null;
                            break;
                    }
                    break;
            }
        }

        public void SendMessageToServer(NetOutgoingMessage msg, NetDeliveryMethod method, int channel)
        {
            ServerConnection.SendMessage(msg, method, channel);
        }


    }
}
