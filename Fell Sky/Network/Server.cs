﻿using System.Collections.Generic;
using FellSky.Core;
using FellSky.Network.Messages;
using FellSky.Utilities;
using Lidgren.Network;

namespace FellSky.Network
{
    public class Server : Peer
    {
        public readonly ServerData ServerData;

        public NetServer NetServer
        {
            get { return (NetServer) NetPeer; }
        }

        public readonly List<NetConnection> Connections = new List<NetConnection>();

        public Server(ServerData data): base(new NetServer(Core.ServerConfiguration))
        {
            this.Log("Starting Fell Sky Server...");
            ServerData = data;
            NetServer.Start();
        }

        protected override void HandleMessage(NetIncomingMessage msg)
        {
            base.HandleMessage(msg);

            switch (msg.MessageType)
            {
                case NetIncomingMessageType.DiscoveryRequest:
                    var response = NetPeer.CreateMessage();
                    ServerData.WriteToMessage(response);
                    // Send the response to the sender of the request
                    NetPeer.SendDiscoveryResponse(response, msg.SenderEndPoint);                       
                    this.Log("{0} trying to discover this server, sending response.", msg.SenderEndPoint.ToString());
                    break;
                case NetIncomingMessageType.ConnectionApproval:
                    ApproveClientConnectionRequest(msg);
                    break;
            }
        }

        private void ApproveClientConnectionRequest(NetIncomingMessage msg)
        {
            
            var joinMsg = new TryJoinGameMessage(msg);

            this.Log("{0} from {1} is trying to establish connection.", joinMsg.Username, msg.SenderEndPoint);

            //TODO: check for account/passsword here
            if (string.IsNullOrWhiteSpace(joinMsg.Username))
            {
                msg.SenderConnection.Deny("Invalid username.");
                return;
            }

            if (joinMsg.Password != ServerData.Password)
            {
                msg.SenderConnection.Deny("Invalid password.");
                return;
            }

            var player = new Player(joinMsg);
            Players.Add(player);

            Connections.Add(msg.SenderConnection);

            var hail = NetServer.CreateMessage();
            var acceptMsg = new ServerJoinGameAcceptMessage(ServerData, player);
            acceptMsg.WriteToMessage(hail);
            msg.SenderConnection.Approve(hail);
            
        }

        public List<Player> Players=new List<Player>();

        public void BroadcastMessageToClients(NetOutgoingMessage msg, NetDeliveryMethod method, int sequenceChannel)
        {
            NetServer.SendMessage(msg, Connections, method, sequenceChannel);
        }
    }
}
