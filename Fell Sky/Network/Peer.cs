﻿using System;
using System.Collections.Generic;
using FellSky.Core;
using Lidgren.Network;

namespace FellSky.Network
{
    public abstract class Peer : IUpdateable
    {
        public Dictionary<MessageCode, Dictionary<int, HashSet<INetworkListener>>> Handlers = new Dictionary<MessageCode, Dictionary<int, HashSet<INetworkListener>>>();

        private readonly NetMessageHeader _incomingMsgHeader=new NetMessageHeader();
        private readonly NetMessageHeader _outgoingMsgHeader=new NetMessageHeader();

        protected readonly NetPeer NetPeer;

        protected Peer(NetPeer netPeer)
        {
            NetPeer = netPeer;
        }

        public event Action<TimeSpan> OnUpdate;

        public void Update(TimeSpan timeStep)
        {
            _outgoingMsgHeader.CurrentFrameNum++;
            NetIncomingMessage msg;
            while ((msg = NetPeer.ReadMessage()) != null)
                HandleMessage(msg);
        }

        protected virtual void HandleMessage(NetIncomingMessage msg)
        {
            try
            {
                if (msg.MessageType != NetIncomingMessageType.Data) return;
                msg.ReadAllFields(_incomingMsgHeader);
                var code = _incomingMsgHeader.MessageCode;

                Dictionary<int, HashSet<INetworkListener>> objecthandlers;
                if (Handlers.TryGetValue(code, out objecthandlers))
                {
                    HashSet<INetworkListener> handlers;
                    if (objecthandlers.TryGetValue(_incomingMsgHeader.ObjectID, out handlers))
                        foreach (var networkListener in handlers)
                        {
                            networkListener.HandleNetworkMessage(_incomingMsgHeader, msg);
                        }
                }
            }
            catch
            {
                #if DEBUG
                    throw;
                #endif
            }
        }

        public void RegisterHandler(MessageCode code, int objectID, INetworkListener handler)
        {
            Dictionary<int, HashSet<INetworkListener>> msgHandlers;
            if (!Handlers.TryGetValue(code, out msgHandlers))
            {
                msgHandlers = new Dictionary<int, HashSet<INetworkListener>>();
                Handlers[code] = msgHandlers;
            }

            HashSet<INetworkListener> objHandlers;

            if (!msgHandlers.TryGetValue(objectID, out objHandlers))
            {
                objHandlers=new HashSet<INetworkListener>();
                msgHandlers[objectID] = objHandlers;
            }

            objHandlers.Add(handler);
        }

        public void UnregisterHandler(MessageCode code, int objectID, INetworkListener handler)
        {
            Dictionary<int, HashSet<INetworkListener>> objecthandlers;
            if (Handlers.TryGetValue(code, out objecthandlers))
            {
                HashSet<INetworkListener> handlers;
                if (objecthandlers.TryGetValue(_incomingMsgHeader.ObjectID, out handlers))
                    handlers.Remove(handler);
            }
        }

        public NetOutgoingMessage CreateMessage(INetMessage message)
        {
            _outgoingMsgHeader.MessageNumber++;
            _outgoingMsgHeader.MessageCode = message.MessageCode;
            //_outgoingMsgHeader.SenderID = GetHashCode();
            var msg = NetPeer.CreateMessage();
            _outgoingMsgHeader.WriteToMessage(msg);
            message.WriteToMessage(msg);
            return msg;
        }
    }
}
