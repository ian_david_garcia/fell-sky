﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace FellSky.Network
{
    public class NetMessageHeader: INetMessage
    {
        public UInt64 CurrentFrameNum;
        public UInt64 MessageNumber;
        public MessageCode MessageCode { get; set; }
        public int ObjectID = 0;
        
        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(CurrentFrameNum);
            msg.Write((int)MessageCode);
            msg.Write(MessageNumber);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            CurrentFrameNum = msg.ReadUInt64();
            MessageNumber = msg.ReadUInt64();
            MessageCode = (MessageCode) msg.ReadInt32();
        }
    }
}
