﻿using Lidgren.Network;

namespace FellSky.Network
{
    public static class Core
    {
        public const int ServerPort = 41828;
        public const int ClientPort = 41829;



        public static NetPeerConfiguration ServerConfiguration = new NetPeerConfiguration("FellSky")
        {
            Port = ServerPort
        };
        public static NetPeerConfiguration ClientConfiguration = new NetPeerConfiguration("FellSky")
        {
            Port = ClientPort
        };

        static Core()
        {
            ServerConfiguration.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            ClientConfiguration.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
        }

        public static Server Server;
        public static Client Client;

        public static void CreateServer()
        {
            
        }

        public static void CreateClient()
        {
            
        }
    }
}
