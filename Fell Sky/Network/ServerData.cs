﻿using System;
using System.Net;
using Lidgren.Network;

namespace FellSky.Network
{
    public class ServerData: INetMessage
    {
        public string Name;
        public string Password;
        public Guid ID;
        public string MapName;
        // TODO: More Server data such as current map name, game progress, etc.


        public IPEndPoint Address { get; set; }
        public MessageCode MessageCode { get { return MessageCode.ServerData; } }

        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(ID.ToString());
            msg.Write(Name);
            msg.Write(MapName);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            ID = new Guid(msg.ReadString());
            Name = msg.ReadString();
            MapName = msg.ReadString();
            Address = msg.SenderEndPoint;
        }
    }

    
}
