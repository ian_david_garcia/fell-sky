﻿using Lidgren.Network;

namespace FellSky.Network
{
    public interface INetMessage
    {
        MessageCode MessageCode { get; }

        void WriteToMessage(NetOutgoingMessage msg);
        void ReadFromMessage(NetIncomingMessage msg);
    }
}