﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace FellSky.Network.Messages
{
    public class TryJoinGameMessage: INetMessage
    {
        public string Username {
            get { return _username; }
        }
        private string _username, _password;
        public IPEndPoint Address;

        public TryJoinGameMessage(string username, string password)
        {
            _username = username;
            _password = password;
        }

        public TryJoinGameMessage(NetIncomingMessage msg)
        {
            ReadFromMessage(msg);
        }

        public MessageCode MessageCode {
            get { return MessageCode.ClientJoinGame; }
        }

        public string Password { get { return _password; } }

        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(_username);
            msg.Write(_password);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            _username = msg.ReadString();
            _password = msg.ReadString();
            Address = msg.SenderEndPoint;
        }
    }
}
