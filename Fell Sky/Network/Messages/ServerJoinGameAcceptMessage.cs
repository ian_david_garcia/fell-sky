﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FellSky.Core;
using Lidgren.Network;

namespace FellSky.Network.Messages
{
    public class ServerJoinGameAcceptMessage: INetMessage
    {
        public Player Player=new Player();
        public ServerData ServerData = new ServerData();

        public ServerJoinGameAcceptMessage(NetIncomingMessage msg)
        {
            ReadFromMessage(msg);
        }

        public ServerJoinGameAcceptMessage(ServerData serverData, Player player)
        {
            ServerData = serverData;
            Player = player;
        }

        public MessageCode MessageCode {
            get { return MessageCode.ServerJoinGameAccept;}
        }
        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(Player.ID);
            msg.Write(Player.Name);
            msg.Write(Player.IsReady);
            msg.Write(Player.CurrentShipArchetype);
            msg.Write(ServerData.ID.ToString());
            msg.Write(ServerData.Name);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            Player.ID = msg.ReadInt32();
            Player.Name = msg.ReadString();
            Player.IsReady = msg.ReadBoolean();
            Player.CurrentShipArchetype = msg.ReadString();
            ServerData.ID = new Guid(msg.ReadString());
            ServerData.Name = msg.ReadString();
            ServerData.Address = msg.SenderEndPoint;            
        }
    }
}
