﻿using Lidgren.Network;

namespace FellSky.Network.Messages
{
    public class PlayerReadyMessage: INetMessage
    {
        public int PlayerID;
        public bool PlayerIsReady;
        public MessageCode MessageCode { get { return MessageCode.PlayerReady; } }
        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(PlayerID);
            msg.Write(PlayerIsReady);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            PlayerID = msg.ReadInt32();
            PlayerIsReady = msg.ReadBoolean();
        }
    }
}
