﻿using FellSky.Core;
using Lidgren.Network;
using SFML;

namespace FellSky.Network.Messages
{
    public class SpatialUpdateMessage: INetMessage
    {
        public MessageCode MessageCode {
            get { return MessageCode.SpatialUpdate; }
        }

        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(Position.X);
            msg.Write(Position.Y);
            msg.Write(Scale.X);
            msg.Write(Scale.Y);
            msg.Write(Rotation);
            msg.Write(Origin.X);
            msg.Write(Origin.Y);
            msg.Write(AngularVelocity);
            msg.Write(LinearVelocity.X);
            msg.Write(LinearVelocity.Y);

        }
        public void ReadFromMessage(NetIncomingMessage msg)
        {
            Position.X= msg.ReadFloat();
            Position.Y= msg.ReadFloat();
            Scale.X= msg.ReadFloat();
            Scale.Y= msg.ReadFloat();
            Rotation= msg.ReadFloat();
            Origin.X= msg.ReadFloat();
            Origin.Y= msg.ReadFloat();
            AngularVelocity= msg.ReadFloat();
            LinearVelocity.X= msg.ReadFloat();
            LinearVelocity.Y= msg.ReadFloat();
        }

        public Vector2f Position, Scale, Origin;
        public float Rotation;
        public Vector2f LinearVelocity;
        public float AngularVelocity;

        public void GetDataFrom(IMoveable obj)
        {
            Position = obj.Position;
            Rotation = obj.Rotation;
            Scale = obj.Scale;
            Origin = obj.Origin;
            AngularVelocity = obj.AngularVelocity;
            LinearVelocity = obj.LinearVelocity;
        }

        public void AssignDataTo(IMoveable obj)
        {
            obj.Position = Position;
            obj.Rotation = Rotation;
            obj.Scale = Scale;
            obj.Origin = Origin;
            obj.AngularVelocity = AngularVelocity;
            obj.LinearVelocity = LinearVelocity;
        }
    }
}
