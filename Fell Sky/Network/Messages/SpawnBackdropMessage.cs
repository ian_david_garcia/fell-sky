﻿using Lidgren.Network;

namespace FellSky.Network.Messages
{
    public class SpawnBackdropMessage: INetMessage
    {
        public MessageCode MessageCode {
            get { return MessageCode.SpawnBackdrop; }
        }

        public float Parallax;

        public string BackdropID;

        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(BackdropID);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            BackdropID = msg.ReadString();
        }
    }
}
