﻿using FellSky.Core;
using FellSky.Entities;
using FellSky.Utilities;
using Lidgren.Network;
using SFML;

namespace FellSky.Network.Messages
{
    public class ShipSpawnMessage : INetMessage
    {
        public Ship Ship;

        public MessageCode MessageCode
        {
            get { return MessageCode.SpawnShip; }
        }

        public Vector2f Position;

        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(Ship.ArchetypeID);
            msg.Write(Ship.ID);
            msg.Write(Ship.GivenName);
            msg.Write(Ship.Position);
            msg.Write(Ship.Rotation);
            msg.Write(Ship.Scale);
            msg.Write(Ship.Origin);

            foreach (var part in Ship.Parts)
            {
                msg.Write(part.ID);
                msg.Write(part.CurrentHealth);
            }
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            //if (Ship == null) return;
            var archetypeID = msg.ReadString();

            Ship = Archetypes<Ship>.CreateObject(archetypeID);

            Ship.ID = msg.ReadInt32();
            Ship.GivenName = msg.ReadString();
            Ship.Position = msg.ReadVector2f();
            Ship.Rotation = msg.ReadFloat();
            Ship.Scale = msg.ReadVector2f();
            Ship.Origin = msg.ReadVector2f();

            foreach (var part in Ship.Parts)
            {
                part.ID = msg.ReadInt32();
                part.CurrentHealth = msg.ReadFloat();
            }
        }
    }
}