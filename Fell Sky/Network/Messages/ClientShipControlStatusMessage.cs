﻿using Lidgren.Network;

namespace FellSky.Network.Messages
{
    public class ClientShipControlStatusMessage: INetMessage
    {
        public bool ThrustForward, ThrustBack;
        public bool ThrustLeft, ThrustRight;
        public bool TurnLeft, TurnRight;


        public MessageCode MessageCode { get { return MessageCode.ClientShipControlStatus; }}
        public void WriteToMessage(NetOutgoingMessage msg)
        {
            msg.Write(ThrustForward);
            msg.Write(ThrustBack);
            msg.Write(ThrustLeft);
            msg.Write(ThrustRight);
            msg.Write(TurnLeft);
            msg.Write(TurnRight);
        }

        public void ReadFromMessage(NetIncomingMessage msg)
        {
            ThrustForward=msg.ReadBoolean();
            ThrustBack=msg.ReadBoolean();
            ThrustLeft=msg.ReadBoolean();
            ThrustRight=msg.ReadBoolean();
            TurnLeft=msg.ReadBoolean();
            TurnRight=msg.ReadBoolean();
        }
    }
}
