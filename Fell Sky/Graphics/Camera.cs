﻿using System;
using FellSky.Core;
using SFML;
using SFML.Graphics;

namespace FellSky.Graphics
{
    public class Camera: SpatialEntity, IMoveable
    {
        public Vector2f LinearVelocity { get; set; }
        public float AngularVelocity { get; set; }

        public void Shake(TimeSpan duration, float amount)
        {
            
        }

        public void ApplyCameraTransform(ref Transform transform, float parallax)
        {
            
        }
    }
}
