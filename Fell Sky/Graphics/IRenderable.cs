﻿using System;
using System.Collections.Generic;
using SFML.Graphics;

namespace FellSky.Graphics
{

    public interface IRenderable
    {
        LinkedListNode<IRenderable> RenderNode { get; set; }

        event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnBeforeRender;
        event Action<object, TimeSpan, RenderTarget, Camera, RenderStates, int> OnAfterRender;

        /// <summary>
        /// Renders this object.
        /// </summary>
        /// <param name="timeSpan">The timestep.</param>
        /// <param name="target">The current render target.</param>
        /// <param name="camera">The active camera.</param>
        /// <param name="states">The renderstates to use.</param>
        /// <param name="renderMode">An optional value specifying how to render the object. The meaning of the rendermode values are dependent on the type of object.</param>
        void Render(TimeSpan timeSpan, RenderTarget target, Camera camera, RenderStates states, int renderMode=0);
    }
}
