﻿using System;
using System.Collections.Generic;
using FellSky.Utilities;
using SFML;
using SFML.Graphics;

namespace FellSky.Graphics
{
    /// <summary>
    ///     Provides optimized drawing of sprites
    /// </summary>
    public class SpriteBatch : Drawable
    {
        private readonly int _max;
        private readonly List<QueueItem> _textures = new List<QueueItem>();

        private bool _active;
        private Texture _activeTexture;
        private uint _queueCount;
        private BlendMode _activeBlendMode = BlendMode.Alpha;
        private Vertex[] _vertices = new Vertex[100*4];
        

        public SpriteBatch(int maxCapacity = 100000)
        {
            _max = maxCapacity*4;
        }

        public int Count { get; private set; }
        public int BatchCount {
            get { return _textures.Count; }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            if (_active) throw new Exception("Call End first.");

            uint index = 0;
            foreach (QueueItem item in _textures)
            {
                //Debug.Assert(item.Count > 0);
                states.Texture = item.Texture;
                states.BlendMode = item.BlendMode;
                //Console.WriteLine(states.BlendMode);
                target.Draw(_vertices, index, item.Count, PrimitiveType.Quads, states);
                //target.Draw(vertices,PrimitiveType.Quads, states);
                index += item.Count;
            }
        }

        public void Begin()
        {
            if (_active) throw new Exception("Already active");
            Count = 0;
            _textures.Clear();
            _active = true;

            _activeTexture = null;
            _activeBlendMode=BlendMode.Alpha;
        }

        public void End()
        {
            if (!_active) throw new Exception("Call end first.");
            Enqueue();
            _active = false;
        }

        private void Enqueue()
        {
            if (_queueCount > 0)
                _textures.Add(new QueueItem
                    {
                        Texture = _activeTexture,
                        BlendMode = _activeBlendMode,
                        Count = _queueCount
                    });
            _queueCount = 0;
        }

        private int Create(Texture texture, BlendMode blendMode = BlendMode.Alpha)
        {
            if (!_active) throw new Exception("Call Begin first.");

            if (texture != _activeTexture || blendMode!=_activeBlendMode)
            {
                Enqueue();
                _activeTexture = texture;
                _activeBlendMode = blendMode;
            }

            if (Count >= (_vertices.Length/4))
            {
                if (_vertices.Length < _max)
                    Array.Resize(ref _vertices, Math.Min(_vertices.Length*2, _max));
                else throw new Exception("Too many items");
            }

            _queueCount += 4;
            return 4*Count++;
        }

        public void Draw(IEnumerable<Sprite> sprites, BlendMode blendMode = BlendMode.Alpha)
        {
            foreach (Sprite s in sprites)
                Draw(s,blendMode);
        }

        public void Draw(Sprite sprite, BlendMode blendMode=BlendMode.Alpha)
        {
            Draw(sprite.Texture, sprite.Position, sprite.TextureRect, sprite.Color, sprite.Scale, sprite.Origin,
                 sprite.Rotation,blendMode);
        }

        public unsafe void Draw(Texture texture, Vector2f position, IntRect rec, Color color, Vector2f scale,
                                Vector2f origin, float rotation = 0, BlendMode blendMode = BlendMode.Alpha)
        {
            int index = Create(texture,blendMode);
            //FloatMath.SinCos(rotation, out sin, out cos);

            rotation = rotation.ToRadians();
            var sin = (float) Math.Sin(rotation);
            var cos = (float) Math.Cos(rotation);
            

            float pX = -origin.X*scale.X;
            float pY = -origin.Y*scale.Y;
            scale.X *= rec.Width;
            scale.Y *= rec.Height;

            fixed (Vertex* fptr = _vertices)
            {
                Vertex* ptr = fptr + index;

                ptr->Position.X = pX*cos - pY*sin + position.X;
                ptr->Position.Y = pX*sin + pY*cos + position.Y;
                ptr->TexCoords.X = rec.Left;
                ptr->TexCoords.Y = rec.Top;
                ptr->Color = color;

                ptr++;

                pX += scale.X;
                ptr->Position.X = pX*cos - pY*sin + position.X;
                ptr->Position.Y = pX*sin + pY*cos + position.Y;
                ptr->TexCoords.X = rec.Right;
                ptr->TexCoords.Y = rec.Top;
                ptr->Color = color;

                ptr++;

                pY += scale.Y;
                ptr->Position.X = pX*cos - pY*sin + position.X;
                ptr->Position.Y = pX*sin + pY*cos + position.Y;
                ptr->TexCoords.X = rec.Right;
                ptr->TexCoords.Y = rec.Bottom;
                ptr->Color = color;

                ptr++;

                pX -= scale.X;
                ptr->Position.X = pX*cos - pY*sin + position.X;
                ptr->Position.Y = pX*sin + pY*cos + position.Y;
                ptr->TexCoords.X = rec.Left;
                ptr->TexCoords.Y = rec.Bottom;

                ptr->Color = color;
            }
        }

        public unsafe void Draw(Sprite spr, Transform xform, BlendMode blendMode = BlendMode.Alpha)
        {
            int index = Create(spr.Texture,blendMode);
            var color = spr.Color;
            xform *= spr.Transform;

            //FloatMath.SinCos(rotation, out sin, out cos);
            var rec = spr.TextureRect;

            fixed (Vertex* fptr = _vertices)
            {
                Vertex* ptr = fptr + index;

                ptr->Position = xform.TransformPoint(rec.Left,rec.Top);
                ptr->TexCoords.X = rec.Left;
                ptr->TexCoords.Y = rec.Top;
                ptr->Color = color;

                ptr++;

                ptr->Position = xform.TransformPoint(rec.Right,rec.Top);
                ptr->TexCoords.X = rec.Right;
                ptr->TexCoords.Y = rec.Top;
                ptr->Color = color;

                ptr++;

                ptr->Position = xform.TransformPoint(rec.Right, rec.Bottom); 
                ptr->TexCoords.X = rec.Right;
                ptr->TexCoords.Y = rec.Bottom;
                ptr->Color = color;

                ptr++;

                ptr->Position = xform.TransformPoint(rec.Left, rec.Bottom); 
                ptr->TexCoords.X = rec.Left;
                ptr->TexCoords.Y = rec.Bottom;
                ptr->Color = color;
            }
        }

        public unsafe void Draw(Texture texture, FloatRect rec, IntRect src, Color color, BlendMode blendMode = BlendMode.Alpha)
        {
            int index = Create(texture,blendMode);

            fixed (Vertex* fptr = _vertices)
            {
                Vertex* ptr = fptr + index;

                ptr->Position.X = rec.Left;
                ptr->Position.Y = rec.Top;
                ptr->TexCoords.X = src.Left;
                ptr->TexCoords.Y = src.Top;
                ptr->Color = color;
                ptr++;

                ptr->Position.X = rec.Right;
                ptr->Position.Y = rec.Top;
                ptr->TexCoords.X = src.Right;
                ptr->TexCoords.Y = src.Top;
                ptr->Color = color;
                ptr++;

                ptr->Position.X = rec.Right;
                ptr->Position.Y = rec.Bottom;
                ptr->TexCoords.X = src.Right;
                ptr->TexCoords.Y = src.Bottom;
                ptr->Color = color;
                ptr++;

                ptr->Position.X = rec.Left;
                ptr->Position.Y = rec.Bottom;
                ptr->TexCoords.X = src.Left;
                ptr->TexCoords.Y = src.Bottom;
                ptr->Color = color;
            }
        }

        public void Draw(Texture texture, FloatRect rec, Color color, BlendMode blendMode = BlendMode.Alpha)
        {
            int width = 1, height = 1;
            if (texture != null)
            {
                width = (int) texture.Size.X;
                height = (int) texture.Size.Y;
            }
            Draw(texture, rec, new IntRect(0, 0, width, height), color,blendMode);
        }

        public void Draw(Texture texture, Vector2f pos, Color color, BlendMode blendMode = BlendMode.Alpha)
        {
            if (texture == null) throw new ArgumentNullException();
            var width = (int) texture.Size.X;
            var height = (int) texture.Size.Y;
            Draw(texture, new FloatRect(pos.X, pos.Y, width, height), new IntRect(0, 0, width, height), color,blendMode);
        }

        private struct QueueItem
        {
            public uint Count;
            public Texture Texture;
            public BlendMode BlendMode;
        }
    }
}