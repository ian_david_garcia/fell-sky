﻿using FellSky.Editor.Core;

namespace FellSky.Editor.Utilities
{
    partial class SpriteDropdown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DropDown = new System.Windows.Forms.ComboBox();
            this.currentSpriteDisplay = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // DropDown
            // 
            this.DropDown.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.DropDown.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.DropDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DropDown.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.DropDown.FormattingEnabled = true;
            this.DropDown.Location = new System.Drawing.Point(0, 73);
            this.DropDown.Name = "DropDown";
            this.DropDown.Size = new System.Drawing.Size(74, 21);
            this.DropDown.TabIndex = 0;
            // 
            // currentSpriteDisplay
            // 
            this.currentSpriteDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.currentSpriteDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.currentSpriteDisplay.Location = new System.Drawing.Point(1, 1);
            this.currentSpriteDisplay.Name = "currentSpriteDisplay";
            this.currentSpriteDisplay.Size = new System.Drawing.Size(72, 72);
            this.currentSpriteDisplay.TabIndex = 1;
            // 
            // SpriteDropdown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.currentSpriteDisplay);
            this.Controls.Add(this.DropDown);
            this.Name = "SpriteDropdown";
            this.Size = new System.Drawing.Size(74, 94);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel currentSpriteDisplay;
        public System.Windows.Forms.ComboBox DropDown;



    }
}
