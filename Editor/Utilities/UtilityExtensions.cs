﻿using System.Drawing;
using SFML;

namespace FellSky.Editor.Utilities
{
    public static class UtilityExtensions
    {
        public static Point ToNetPoint(this Vector2f vec)
        {
            return new Point((int) vec.X,(int) vec.Y);
        }

        public static Vector2f ToSFMLVector2f(this Point vec)
        {
            return new Vector2f(vec.X,vec.Y);
        }
        
        public static Vector2i ToSFMLVector2i(this Point vec)
        {
            return new Vector2i(vec.X,vec.Y);
        }

        public static Vector2u ToSFMLVector2u(this Point vec)
        {
            return new Vector2u((uint) vec.X, (uint) vec.Y);
        }


        public static Rectangle ToNetRect(this IntRect rect)
        {
            return new Rectangle(rect.Left,rect.Top,rect.Width,rect.Height);
        }

        public static IntRect ToSFMLIntRect(this Rectangle rect)
        {
            return new IntRect(rect.Left,rect.Top,rect.Width,rect.Height);
        }

        public static FloatRect ToSFMLFloatRect(this Rectangle rect)
        {
            return new FloatRect(rect.Left, rect.Top, rect.Width, rect.Height);
        }

    }
}
