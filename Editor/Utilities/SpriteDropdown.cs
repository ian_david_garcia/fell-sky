﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using FellSky.AssetManagement;
using FellSky.Editor.Core;

namespace FellSky.Editor.Utilities
{
    public partial class SpriteDropdown : UserControl
    {
        public SpriteAsset[] Assets
        {
            get { return _assets; }
            set
            {
                _assets = value;
                _bitmaps = _assets.Select(p => BitmapAsset.Get(p.TextureID)).ToArray();
                DropDown.DataSource = Assets;
            }
        }

        public SpriteAsset Value
        {
            get { return _value; }
            set
            {
                _value = value;
                currentSpriteDisplay.Invalidate();
            }
        }

        private Bitmap[] _bitmaps;
        private SpriteAsset[] _assets=new SpriteAsset[0];
        private SpriteAsset _value;

        public SpriteDropdown()
        {
            InitializeComponent();
            
            DropDown.MeasureItem += (o, e) =>
            {
                e.ItemWidth = Assets[e.Index].Value.TextureRect.Width+4;
                e.ItemHeight = Assets[e.Index].Value.TextureRect.Height+4;
            };

            DropDown.DrawItem += (o, e) =>
            {
                e.DrawBackground();
                e.DrawFocusRectangle();

                var spr = Assets[e.Index].Value;

                var x = spr.TextureRect.Width;
                var y = spr.TextureRect.Height;
                var rect = new Rectangle(1, e.Bounds.Top + 1, e.Bounds.Width, e.Bounds.Height);
                e.Graphics.DrawImage(_bitmaps[e.Index], e.Bounds.Left+2, e.Bounds.Top+2, spr.TextureRect.ToNetRect(), GraphicsUnit.Pixel);

            };

            DropDown.SelectedValueChanged += (o, e) =>
            {
                Value = (SpriteAsset) DropDown.SelectedValue;
            };

            DropDown.TextChanged += (o, e) =>
            {

            };

            currentSpriteDisplay.Paint += (o, e) =>
            {
                if (Value == null) return;
                var b = (Control)o;
                var sprite = Value.Value;
                var x = b.Width / 2 - sprite.TextureRect.Width / 2;
                var y = b.Height / 2 - sprite.TextureRect.Height / 2;

                e.Graphics.DrawImage(BitmapAsset.Get(Value.TextureID), x, y, sprite.TextureRect.ToNetRect(), GraphicsUnit.Pixel);
            };
        }
    }
}
