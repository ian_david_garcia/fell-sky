﻿using System;
using System.IO;
using System.Windows.Forms;
using FellSky.AssetManagement;
using FellSky.Editor.Core;
using FellSky.Editor.Main;

namespace FellSky.Editor
{
    public static class Program
    {
        public static void Start()
        {
            Initialize();
            Application.EnableVisualStyles();
            Application.Run(new MainForm());
            
        }

        private static void Initialize()
        {
            Directory.SetCurrentDirectory(Game.DataDirectory);
            AssetManager.Register("Include", typeof(IncludeAsset));
            AssetManager.Register("String", typeof(StringAsset));
            AssetManager.Register("Xml", typeof(XmlAsset));
            AssetManager.Register("Sprite", typeof(SpriteAsset));
            AssetManager.Register("Texture", typeof(TextureAsset));
            
            // Don't load fonts and UIs

            //AssetManager.Register("UIDocument", typeof(UIDocumentAsset));
            //AssetManager.Register("Font", typeof(FontAsset));
            
            AssetManager.LoadFromFile(Game.PreloadedAssetsFile);
            AssetManager.LoadFromFile(Game.MainAssetsFile);
            BitmapAsset.LoadAll();
            AssetManager.PrintAssetCounts();
        }

        [STAThread]
        private static void Main(string[] args)
        {
            Start();

        }
    }
}
