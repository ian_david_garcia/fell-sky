﻿namespace FellSky.Editor.Main
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shipEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.particleEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archetypeEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shapeEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editorsToolStripMenuItem,
            this.windowToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.windowToolStripMenuItem;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1070, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editorsToolStripMenuItem
            // 
            this.editorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shipEditorToolStripMenuItem,
            this.mapEditorToolStripMenuItem,
            this.particleEditorToolStripMenuItem,
            this.archetypeEditorToolStripMenuItem,
            this.spriteEditorToolStripMenuItem,
            this.shapeEditorToolStripMenuItem});
            this.editorsToolStripMenuItem.Name = "editorsToolStripMenuItem";
            this.editorsToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.editorsToolStripMenuItem.Text = "Editors";
            // 
            // shipEditorToolStripMenuItem
            // 
            this.shipEditorToolStripMenuItem.Name = "shipEditorToolStripMenuItem";
            this.shipEditorToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.shipEditorToolStripMenuItem.Text = "Ship Editor";
            // 
            // particleEditorToolStripMenuItem
            // 
            this.particleEditorToolStripMenuItem.Name = "particleEditorToolStripMenuItem";
            this.particleEditorToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.particleEditorToolStripMenuItem.Text = "Particle Editor";
            // 
            // archetypeEditorToolStripMenuItem
            // 
            this.archetypeEditorToolStripMenuItem.Name = "archetypeEditorToolStripMenuItem";
            this.archetypeEditorToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.archetypeEditorToolStripMenuItem.Text = "Archetype Editor";
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.MergeIndex = 1000;
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "Window";
            // 
            // mapEditorToolStripMenuItem
            // 
            this.mapEditorToolStripMenuItem.Name = "mapEditorToolStripMenuItem";
            this.mapEditorToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.mapEditorToolStripMenuItem.Text = "Map Editor";
            // 
            // spriteEditorToolStripMenuItem
            // 
            this.spriteEditorToolStripMenuItem.Name = "spriteEditorToolStripMenuItem";
            this.spriteEditorToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.spriteEditorToolStripMenuItem.Text = "Sprite Editor";
            // 
            // shapeEditorToolStripMenuItem
            // 
            this.shapeEditorToolStripMenuItem.Name = "shapeEditorToolStripMenuItem";
            this.shapeEditorToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.shapeEditorToolStripMenuItem.Text = "Shape Editor";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 527);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Fell Sky Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shipEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem particleEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archetypeEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spriteEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shapeEditorToolStripMenuItem;
    }
}