﻿using System.Windows.Forms;


namespace FellSky.Editor.Main
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            shipEditorToolStripMenuItem.Click += (o, e) => AddChildWindow(new ShipEditor.ShipEditorForm());
            archetypeEditorToolStripMenuItem.Click += (o, e) => AddChildWindow(new ArchetypeEditor.ArchetypeEditorForm());
            mapEditorToolStripMenuItem.Click += (o, e) => AddChildWindow(new MapEditor.MapEditorForm());
            particleEditorToolStripMenuItem.Click += (o, e) => AddChildWindow(new ParticleEditor.ParticleEditorForm());
            spriteEditorToolStripMenuItem.Click += (o, e) => AddChildWindow(new SpriteSheetEditor.SpriteSheetEditorForm());
            shapeEditorToolStripMenuItem.Click += (o, e) => AddChildWindow(new ShapeEditor.ShapeEditorForm());
        }

        private void AddChildWindow(Form form)
        {
            form.MdiParent = this;
            form.Show();
        }
    }
}
