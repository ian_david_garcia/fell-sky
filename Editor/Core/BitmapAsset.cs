﻿using System.Linq;
using System.Xml;
using FellSky.AssetManagement;

namespace FellSky.Editor.Core
{
    public class BitmapAsset: Asset<System.Drawing.Bitmap>
    {

        public BitmapAsset(TextureAsset textureAsset): base(null)
        {
            ID = textureAsset.ID;
            Value = new System.Drawing.Bitmap(textureAsset.Filename);
        }

        public BitmapAsset(XmlNode xml) : base(xml)
        {
        }

        public static void LoadAll()
        {
            foreach (var bitmap in TextureAsset.Assets.Values.Select(texture => new BitmapAsset((TextureAsset)texture)))
            {
                Assets[bitmap.ID] = bitmap;
            }
        }
    }
}
