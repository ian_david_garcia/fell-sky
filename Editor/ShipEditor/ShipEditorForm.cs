﻿using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using FellSky.AssetManagement;
using FellSky.AssetManagement;
using FellSky.Core;
using FellSky.Entities;
using FellSky.Editor.ShipEditor.Operations;
using FellSky.Editor.Utilities;
using FellSky.Entities.ShipParts;
using SFML;
using SFML.Graphics;
using Color = System.Drawing.Color;

namespace FellSky.Editor.ShipEditor
{
    public partial class ShipEditorForm : Form
    {
        
        public Color CurrentColor;
        public World ClientWorld;
        public Vector2f ShipViewMousePos;
        public Ship Ship;
        public List<ShipPart> SelectedParts = new List<ShipPart>();
        public IShipEditorOperation CurrentOperation;
        public World World;

        public RenderWindow RenderWindow;

        public ShipEditorForm()
        {
            InitializeComponent();
        }

        private void LoadForm(object sender, System.EventArgs e)
        {
            menuStrip1.Visible = false;
            InitializeHullButtons();
            InitializeTurretButtons();
            InitializeUI();
            RenderWindow = new RenderWindow(ShipDisplayControl.Handle);
            World = new World();
            CreateNewShip();
        }

        private void InitializeUI()
        {
            otherDeviceSpriteBox.Assets = SpriteAsset.Assets.Values.Cast<SpriteAsset>().Where(p=>p.Type=="hull").ToArray();
            otherDeviceSpriteBox.DropDown.DropDownWidth = 120;
            otherDeviceSpriteBox.DropDown.DropDownHeight = 200;
        }

        private void InitializeTurretButtons()
        {

            var panel = turretButtonPanel;

            foreach (var sprite in SpriteAsset.Assets.Values.Cast<SpriteAsset>()
                .Where(a => a.Type == "turret"))
            {
                var button = new Button { Width = 64, Height = 64 };
                var hull1 = sprite;


                button.Paint += (o, e) =>
                {
                    var b = (Button) o;
                    var spr = hull1.Value;
                    var x = b.Width / 2 - spr.TextureRect.Width / 2;
                    var y = b.Height / 2 - spr.TextureRect.Height / 2;

                    e.Graphics.DrawImage(Asset<Bitmap>.Get(hull1.TextureID), x, y, spr.TextureRect.ToNetRect(), GraphicsUnit.Pixel);
                };

                button.Click += (o, e) => CreatePart<Turret>(hull1.ID);

                formToolTip.SetToolTip(button, hull1.ID);
                panel.Controls.Add(button);
            }
        }

        private void InitializeHullButtons()
        {
            var myTi = new CultureInfo("en-US", false).TextInfo;
            foreach (var spriteCategory in SpriteAsset.Assets.Values.Cast<SpriteAsset>()
                .Where(a=>a.Type=="hull")
                .GroupBy(a=>a.Subtype))
            {
                var page = new TabPage(myTi.ToTitleCase(spriteCategory.Key));
                hullTabControl.TabPages.Add(page);
                var panel = new FlowLayoutPanel {Dock = DockStyle.Fill, AutoScroll = true};
                page.Controls.Add(panel);
                
               
                foreach (var spriteAsset in spriteCategory)
                {
                    var button = new Button { Width = 64, Height = 64 };
                    var hull1 = spriteAsset;


                    button.Paint += (o, e) =>
                    {
                        var b = (Button) o;
                        var sprite = hull1.Value;
                        var x = b.Width / 2 - sprite.TextureRect.Width / 2;
                        var y = b.Height / 2 - sprite.TextureRect.Height / 2;

                        e.Graphics.DrawImage(Asset<Bitmap>.Get(hull1.TextureID), x, y, sprite.TextureRect.ToNetRect(), GraphicsUnit.Pixel);
                    };

                    button.Click += (o, e) => CreatePart<Turret>(hull1.ID);

                    formToolTip.SetToolTip(button, hull1.ID);
                    panel.Controls.Add(button);
                }
            }
        }

        private T CreatePart<T>(string spriteID, object args = null)
                    where T : ShipPart, new()
        {
            //if (IsTestDriving) return default(T);
            var part = new T
            {
                SpriteID = spriteID,
                Position = ShipViewMousePos
            };

            if (args != null)
            {
                var objType = typeof(T);
                foreach (var prop in args.GetType().GetProperties())
                {
                    var objProp = objType.GetProperty(prop.Name);
                    objProp.SetValue(part, prop.GetValue(args));
                }
            }

            Ship.Parts.Add(part);
            SelectedParts.Clear();
            SelectedParts.Add(part);
            //CurrentOperation = new Translate(this);
            //RefreshGroupList();
            return part;
        }



        private void CreateNewShip()
        {
            Ship=new Ship();
            SelectedParts.Clear();

        }
    }
}
