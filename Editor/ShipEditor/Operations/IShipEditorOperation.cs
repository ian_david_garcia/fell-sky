﻿using System.Collections.Generic;
using FellSky.Entities;

namespace FellSky.Editor.ShipEditor.Operations
{
    public interface IShipEditorOperation
    {
        ShipEditorForm Editor { get; set; }
        void Start();
        void Complete();
        void Cancel();
    }
}
