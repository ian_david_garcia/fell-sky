﻿using System.Linq;
using FellSky.AssetManagement;

namespace FellSky.Editor.ShipEditor
{
    partial class ShipEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newShipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newStationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadArchetypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFromFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsArchetypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.testDriveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectInverseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectSameGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.translateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.snapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cloneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mirrorAlongXAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mirrorAlongYAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bringForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bringBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToFrontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToBackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetCameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.hideSelectedPartsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllHiddenPartsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.drawPhysicsDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.colorDesaturateBtn = new System.Windows.Forms.Button();
            this.colorSaturateBtn = new System.Windows.Forms.Button();
            this.colorLightenBtn = new System.Windows.Forms.Button();
            this.colorDarkenBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.colorAlpha = new System.Windows.Forms.NumericUpDown();
            this.PaletteColor16 = new System.Windows.Forms.Button();
            this.PaletteColor8 = new System.Windows.Forms.Button();
            this.PaletteColor12 = new System.Windows.Forms.Button();
            this.PaletteColor4 = new System.Windows.Forms.Button();
            this.PaletteColor14 = new System.Windows.Forms.Button();
            this.PaletteColor6 = new System.Windows.Forms.Button();
            this.PaletteColor10 = new System.Windows.Forms.Button();
            this.PaletteColor2 = new System.Windows.Forms.Button();
            this.PaletteColor15 = new System.Windows.Forms.Button();
            this.PaletteColor7 = new System.Windows.Forms.Button();
            this.PaletteColor11 = new System.Windows.Forms.Button();
            this.PaletteColor3 = new System.Windows.Forms.Button();
            this.PaletteColor13 = new System.Windows.Forms.Button();
            this.PaletteColor5 = new System.Windows.Forms.Button();
            this.PaletteColor9 = new System.Windows.Forms.Button();
            this.PaletteColor1 = new System.Windows.Forms.Button();
            this.PaletteCurrentColor = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.hullTabControl = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.turretButtonPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.otherDeviceSpriteBox = new FellSky.Editor.Utilities.SpriteDropdown();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.ShipDisplayControl = new System.Windows.Forms.Panel();
            this.formToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.loadXMLTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorAlpha)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1031, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newShipToolStripMenuItem,
            this.newStationToolStripMenuItem,
            this.revertToolStripMenuItem,
            this.toolStripSeparator1,
            this.loadArchetypeToolStripMenuItem,
            this.loadFromFileToolStripMenuItem,
            this.loadXMLTemplateToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveToolStripMenuItem,
            this.saveAsArchetypeToolStripMenuItem,
            this.saveAsFileToolStripMenuItem,
            this.toolStripSeparator3,
            this.testDriveToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.fileToolStripMenuItem.MergeIndex = 1;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.fileToolStripMenuItem.Text = "Ship";
            // 
            // newShipToolStripMenuItem
            // 
            this.newShipToolStripMenuItem.Name = "newShipToolStripMenuItem";
            this.newShipToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newShipToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.newShipToolStripMenuItem.Text = "New Ship";
            // 
            // newStationToolStripMenuItem
            // 
            this.newStationToolStripMenuItem.Name = "newStationToolStripMenuItem";
            this.newStationToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.newStationToolStripMenuItem.Text = "New Station";
            // 
            // revertToolStripMenuItem
            // 
            this.revertToolStripMenuItem.Name = "revertToolStripMenuItem";
            this.revertToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.revertToolStripMenuItem.Text = "Revert";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(240, 6);
            // 
            // loadArchetypeToolStripMenuItem
            // 
            this.loadArchetypeToolStripMenuItem.Name = "loadArchetypeToolStripMenuItem";
            this.loadArchetypeToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.loadArchetypeToolStripMenuItem.Text = "Load Archetype";
            // 
            // loadFromFileToolStripMenuItem
            // 
            this.loadFromFileToolStripMenuItem.Name = "loadFromFileToolStripMenuItem";
            this.loadFromFileToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.loadFromFileToolStripMenuItem.Text = "Load From File...";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(240, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsArchetypeToolStripMenuItem
            // 
            this.saveAsArchetypeToolStripMenuItem.Name = "saveAsArchetypeToolStripMenuItem";
            this.saveAsArchetypeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsArchetypeToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.saveAsArchetypeToolStripMenuItem.Text = "Save As Archetype";
            // 
            // saveAsFileToolStripMenuItem
            // 
            this.saveAsFileToolStripMenuItem.Name = "saveAsFileToolStripMenuItem";
            this.saveAsFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsFileToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.saveAsFileToolStripMenuItem.Text = "Save As File...";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(240, 6);
            // 
            // testDriveToolStripMenuItem
            // 
            this.testDriveToolStripMenuItem.Name = "testDriveToolStripMenuItem";
            this.testDriveToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.testDriveToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.testDriveToolStripMenuItem.Text = "Test Drive";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.selectInverseToolStripMenuItem,
            this.selectSameGroupToolStripMenuItem,
            this.toolStripSeparator9,
            this.translateToolStripMenuItem,
            this.rotateToolStripMenuItem,
            this.scaleToolStripMenuItem,
            this.flipHorizontalToolStripMenuItem,
            this.flipVerticalToolStripMenuItem,
            this.snapToolStripMenuItem,
            this.toolStripSeparator5,
            this.deleteToolStripMenuItem,
            this.cloneToolStripMenuItem,
            this.mirrorAlongXAxisToolStripMenuItem,
            this.mirrorAlongYAxisToolStripMenuItem,
            this.toolStripSeparator4,
            this.bringForwardToolStripMenuItem,
            this.bringBackwardToolStripMenuItem,
            this.sendToFrontToolStripMenuItem,
            this.sendToBackToolStripMenuItem});
            this.editToolStripMenuItem.MergeIndex = 2;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            // 
            // selectInverseToolStripMenuItem
            // 
            this.selectInverseToolStripMenuItem.Name = "selectInverseToolStripMenuItem";
            this.selectInverseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.selectInverseToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.selectInverseToolStripMenuItem.Text = "Select Inverse";
            // 
            // selectSameGroupToolStripMenuItem
            // 
            this.selectSameGroupToolStripMenuItem.Name = "selectSameGroupToolStripMenuItem";
            this.selectSameGroupToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.selectSameGroupToolStripMenuItem.Text = "Select same group";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(187, 6);
            // 
            // translateToolStripMenuItem
            // 
            this.translateToolStripMenuItem.Name = "translateToolStripMenuItem";
            this.translateToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.translateToolStripMenuItem.Text = "Translate";
            // 
            // rotateToolStripMenuItem
            // 
            this.rotateToolStripMenuItem.Name = "rotateToolStripMenuItem";
            this.rotateToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.rotateToolStripMenuItem.Text = "Rotate";
            // 
            // scaleToolStripMenuItem
            // 
            this.scaleToolStripMenuItem.Name = "scaleToolStripMenuItem";
            this.scaleToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.scaleToolStripMenuItem.Text = "Scale";
            // 
            // flipHorizontalToolStripMenuItem
            // 
            this.flipHorizontalToolStripMenuItem.Name = "flipHorizontalToolStripMenuItem";
            this.flipHorizontalToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.flipHorizontalToolStripMenuItem.Text = "Flip Horizontal";
            // 
            // flipVerticalToolStripMenuItem
            // 
            this.flipVerticalToolStripMenuItem.Name = "flipVerticalToolStripMenuItem";
            this.flipVerticalToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.flipVerticalToolStripMenuItem.Text = "Flip Vertical";
            // 
            // snapToolStripMenuItem
            // 
            this.snapToolStripMenuItem.Name = "snapToolStripMenuItem";
            this.snapToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.snapToolStripMenuItem.Text = "Snap to Grid";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(187, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // cloneToolStripMenuItem
            // 
            this.cloneToolStripMenuItem.Name = "cloneToolStripMenuItem";
            this.cloneToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.cloneToolStripMenuItem.Text = "Clone";
            // 
            // mirrorAlongXAxisToolStripMenuItem
            // 
            this.mirrorAlongXAxisToolStripMenuItem.Name = "mirrorAlongXAxisToolStripMenuItem";
            this.mirrorAlongXAxisToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.mirrorAlongXAxisToolStripMenuItem.Text = "Mirror along X Axis";
            // 
            // mirrorAlongYAxisToolStripMenuItem
            // 
            this.mirrorAlongYAxisToolStripMenuItem.Name = "mirrorAlongYAxisToolStripMenuItem";
            this.mirrorAlongYAxisToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.mirrorAlongYAxisToolStripMenuItem.Text = "Mirror along Y Axis";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(187, 6);
            // 
            // bringForwardToolStripMenuItem
            // 
            this.bringForwardToolStripMenuItem.Name = "bringForwardToolStripMenuItem";
            this.bringForwardToolStripMenuItem.ShortcutKeyDisplayString = ">";
            this.bringForwardToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.bringForwardToolStripMenuItem.Text = "Bring Forward";
            // 
            // bringBackwardToolStripMenuItem
            // 
            this.bringBackwardToolStripMenuItem.Name = "bringBackwardToolStripMenuItem";
            this.bringBackwardToolStripMenuItem.ShortcutKeyDisplayString = "<";
            this.bringBackwardToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.bringBackwardToolStripMenuItem.Text = "Bring Backward";
            // 
            // sendToFrontToolStripMenuItem
            // 
            this.sendToFrontToolStripMenuItem.Name = "sendToFrontToolStripMenuItem";
            this.sendToFrontToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+>";
            this.sendToFrontToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.sendToFrontToolStripMenuItem.Text = "Send To Front";
            // 
            // sendToBackToolStripMenuItem
            // 
            this.sendToBackToolStripMenuItem.Name = "sendToBackToolStripMenuItem";
            this.sendToBackToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+<";
            this.sendToBackToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.sendToBackToolStripMenuItem.Text = "Send To Back";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetCameraToolStripMenuItem,
            this.toolStripSeparator7,
            this.hideSelectedPartsToolStripMenuItem,
            this.showAllHiddenPartsToolStripMenuItem,
            this.toolStripSeparator6,
            this.drawPhysicsDataToolStripMenuItem,
            this.showGridToolStripMenuItem});
            this.viewToolStripMenuItem.MergeIndex = 3;
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // resetCameraToolStripMenuItem
            // 
            this.resetCameraToolStripMenuItem.Name = "resetCameraToolStripMenuItem";
            this.resetCameraToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.resetCameraToolStripMenuItem.Text = "Reset camera";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(184, 6);
            // 
            // hideSelectedPartsToolStripMenuItem
            // 
            this.hideSelectedPartsToolStripMenuItem.Name = "hideSelectedPartsToolStripMenuItem";
            this.hideSelectedPartsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.hideSelectedPartsToolStripMenuItem.Text = "Hide selected parts";
            // 
            // showAllHiddenPartsToolStripMenuItem
            // 
            this.showAllHiddenPartsToolStripMenuItem.Name = "showAllHiddenPartsToolStripMenuItem";
            this.showAllHiddenPartsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.showAllHiddenPartsToolStripMenuItem.Text = "Show all hidden parts";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(184, 6);
            // 
            // drawPhysicsDataToolStripMenuItem
            // 
            this.drawPhysicsDataToolStripMenuItem.CheckOnClick = true;
            this.drawPhysicsDataToolStripMenuItem.Name = "drawPhysicsDataToolStripMenuItem";
            this.drawPhysicsDataToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.drawPhysicsDataToolStripMenuItem.Text = "Draw physics data";
            // 
            // showGridToolStripMenuItem
            // 
            this.showGridToolStripMenuItem.CheckOnClick = true;
            this.showGridToolStripMenuItem.Name = "showGridToolStripMenuItem";
            this.showGridToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.showGridToolStripMenuItem.Text = "Show grid";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ShipDisplayControl);
            this.splitContainer1.Size = new System.Drawing.Size(1031, 431);
            this.splitContainer1.SplitterDistance = 384;
            this.splitContainer1.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(384, 431);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(376, 405);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Parts";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.colorDesaturateBtn);
            this.panel1.Controls.Add(this.colorSaturateBtn);
            this.panel1.Controls.Add(this.colorLightenBtn);
            this.panel1.Controls.Add(this.colorDarkenBtn);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.colorAlpha);
            this.panel1.Controls.Add(this.PaletteColor16);
            this.panel1.Controls.Add(this.PaletteColor8);
            this.panel1.Controls.Add(this.PaletteColor12);
            this.panel1.Controls.Add(this.PaletteColor4);
            this.panel1.Controls.Add(this.PaletteColor14);
            this.panel1.Controls.Add(this.PaletteColor6);
            this.panel1.Controls.Add(this.PaletteColor10);
            this.panel1.Controls.Add(this.PaletteColor2);
            this.panel1.Controls.Add(this.PaletteColor15);
            this.panel1.Controls.Add(this.PaletteColor7);
            this.panel1.Controls.Add(this.PaletteColor11);
            this.panel1.Controls.Add(this.PaletteColor3);
            this.panel1.Controls.Add(this.PaletteColor13);
            this.panel1.Controls.Add(this.PaletteColor5);
            this.panel1.Controls.Add(this.PaletteColor9);
            this.panel1.Controls.Add(this.PaletteColor1);
            this.panel1.Controls.Add(this.PaletteCurrentColor);
            this.panel1.Location = new System.Drawing.Point(7, 311);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(354, 85);
            this.panel1.TabIndex = 2;
            // 
            // colorDesaturateBtn
            // 
            this.colorDesaturateBtn.Location = new System.Drawing.Point(238, 59);
            this.colorDesaturateBtn.Name = "colorDesaturateBtn";
            this.colorDesaturateBtn.Size = new System.Drawing.Size(76, 23);
            this.colorDesaturateBtn.TabIndex = 8;
            this.colorDesaturateBtn.Text = "Desaturate";
            this.colorDesaturateBtn.UseVisualStyleBackColor = true;
            // 
            // colorSaturateBtn
            // 
            this.colorSaturateBtn.Location = new System.Drawing.Point(160, 59);
            this.colorSaturateBtn.Name = "colorSaturateBtn";
            this.colorSaturateBtn.Size = new System.Drawing.Size(76, 23);
            this.colorSaturateBtn.TabIndex = 7;
            this.colorSaturateBtn.Text = "Saturate";
            this.colorSaturateBtn.UseVisualStyleBackColor = true;
            // 
            // colorLightenBtn
            // 
            this.colorLightenBtn.Location = new System.Drawing.Point(4, 59);
            this.colorLightenBtn.Name = "colorLightenBtn";
            this.colorLightenBtn.Size = new System.Drawing.Size(76, 23);
            this.colorLightenBtn.TabIndex = 6;
            this.colorLightenBtn.Text = "Lighten";
            this.colorLightenBtn.UseVisualStyleBackColor = true;
            // 
            // colorDarkenBtn
            // 
            this.colorDarkenBtn.Location = new System.Drawing.Point(82, 59);
            this.colorDarkenBtn.Name = "colorDarkenBtn";
            this.colorDarkenBtn.Size = new System.Drawing.Size(76, 23);
            this.colorDarkenBtn.TabIndex = 5;
            this.colorDarkenBtn.Text = "Darken";
            this.colorDarkenBtn.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Alpha:";
            // 
            // colorAlpha
            // 
            this.colorAlpha.Location = new System.Drawing.Point(262, 23);
            this.colorAlpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.colorAlpha.Name = "colorAlpha";
            this.colorAlpha.Size = new System.Drawing.Size(45, 20);
            this.colorAlpha.TabIndex = 3;
            this.colorAlpha.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // PaletteColor16
            // 
            this.PaletteColor16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor16.Location = new System.Drawing.Point(231, 29);
            this.PaletteColor16.Name = "PaletteColor16";
            this.PaletteColor16.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor16.TabIndex = 2;
            this.PaletteColor16.Tag = "16";
            this.PaletteColor16.UseVisualStyleBackColor = true;
            // 
            // PaletteColor8
            // 
            this.PaletteColor8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor8.Location = new System.Drawing.Point(231, 4);
            this.PaletteColor8.Name = "PaletteColor8";
            this.PaletteColor8.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor8.TabIndex = 2;
            this.PaletteColor8.Tag = "8";
            this.PaletteColor8.UseVisualStyleBackColor = true;
            // 
            // PaletteColor12
            // 
            this.PaletteColor12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor12.Location = new System.Drawing.Point(131, 29);
            this.PaletteColor12.Name = "PaletteColor12";
            this.PaletteColor12.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor12.TabIndex = 2;
            this.PaletteColor12.Tag = "12";
            this.PaletteColor12.UseVisualStyleBackColor = true;
            // 
            // PaletteColor4
            // 
            this.PaletteColor4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor4.Location = new System.Drawing.Point(131, 4);
            this.PaletteColor4.Name = "PaletteColor4";
            this.PaletteColor4.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor4.TabIndex = 2;
            this.PaletteColor4.Tag = "4";
            this.PaletteColor4.UseVisualStyleBackColor = true;
            // 
            // PaletteColor14
            // 
            this.PaletteColor14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor14.Location = new System.Drawing.Point(181, 29);
            this.PaletteColor14.Name = "PaletteColor14";
            this.PaletteColor14.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor14.TabIndex = 2;
            this.PaletteColor14.Tag = "14";
            this.PaletteColor14.UseVisualStyleBackColor = true;
            // 
            // PaletteColor6
            // 
            this.PaletteColor6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor6.Location = new System.Drawing.Point(181, 4);
            this.PaletteColor6.Name = "PaletteColor6";
            this.PaletteColor6.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor6.TabIndex = 2;
            this.PaletteColor6.Tag = "6";
            this.PaletteColor6.UseVisualStyleBackColor = true;
            // 
            // PaletteColor10
            // 
            this.PaletteColor10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor10.Location = new System.Drawing.Point(81, 29);
            this.PaletteColor10.Name = "PaletteColor10";
            this.PaletteColor10.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor10.TabIndex = 2;
            this.PaletteColor10.Tag = "10";
            this.PaletteColor10.UseVisualStyleBackColor = true;
            // 
            // PaletteColor2
            // 
            this.PaletteColor2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor2.Location = new System.Drawing.Point(81, 4);
            this.PaletteColor2.Name = "PaletteColor2";
            this.PaletteColor2.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor2.TabIndex = 2;
            this.PaletteColor2.Tag = "2";
            this.PaletteColor2.UseVisualStyleBackColor = true;
            // 
            // PaletteColor15
            // 
            this.PaletteColor15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor15.Location = new System.Drawing.Point(206, 29);
            this.PaletteColor15.Name = "PaletteColor15";
            this.PaletteColor15.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor15.TabIndex = 2;
            this.PaletteColor15.Tag = "15";
            this.PaletteColor15.UseVisualStyleBackColor = true;
            // 
            // PaletteColor7
            // 
            this.PaletteColor7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor7.Location = new System.Drawing.Point(206, 4);
            this.PaletteColor7.Name = "PaletteColor7";
            this.PaletteColor7.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor7.TabIndex = 2;
            this.PaletteColor7.Tag = "7";
            this.PaletteColor7.UseVisualStyleBackColor = true;
            // 
            // PaletteColor11
            // 
            this.PaletteColor11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor11.Location = new System.Drawing.Point(106, 29);
            this.PaletteColor11.Name = "PaletteColor11";
            this.PaletteColor11.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor11.TabIndex = 2;
            this.PaletteColor11.Tag = "11";
            this.PaletteColor11.UseVisualStyleBackColor = true;
            // 
            // PaletteColor3
            // 
            this.PaletteColor3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor3.Location = new System.Drawing.Point(106, 4);
            this.PaletteColor3.Name = "PaletteColor3";
            this.PaletteColor3.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor3.TabIndex = 2;
            this.PaletteColor3.Tag = "3";
            this.PaletteColor3.UseVisualStyleBackColor = true;
            // 
            // PaletteColor13
            // 
            this.PaletteColor13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor13.Location = new System.Drawing.Point(156, 29);
            this.PaletteColor13.Name = "PaletteColor13";
            this.PaletteColor13.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor13.TabIndex = 2;
            this.PaletteColor13.Tag = "13";
            this.PaletteColor13.UseVisualStyleBackColor = true;
            // 
            // PaletteColor5
            // 
            this.PaletteColor5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor5.Location = new System.Drawing.Point(156, 4);
            this.PaletteColor5.Name = "PaletteColor5";
            this.PaletteColor5.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor5.TabIndex = 2;
            this.PaletteColor5.Tag = "5";
            this.PaletteColor5.UseVisualStyleBackColor = true;
            // 
            // PaletteColor9
            // 
            this.PaletteColor9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor9.Location = new System.Drawing.Point(56, 29);
            this.PaletteColor9.Name = "PaletteColor9";
            this.PaletteColor9.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor9.TabIndex = 2;
            this.PaletteColor9.Tag = "9";
            this.PaletteColor9.UseVisualStyleBackColor = true;
            // 
            // PaletteColor1
            // 
            this.PaletteColor1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteColor1.Location = new System.Drawing.Point(56, 4);
            this.PaletteColor1.Name = "PaletteColor1";
            this.PaletteColor1.Size = new System.Drawing.Size(24, 24);
            this.PaletteColor1.TabIndex = 2;
            this.PaletteColor1.Tag = "1";
            this.PaletteColor1.UseVisualStyleBackColor = true;
            // 
            // PaletteCurrentColor
            // 
            this.PaletteCurrentColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaletteCurrentColor.Location = new System.Drawing.Point(3, 4);
            this.PaletteCurrentColor.Name = "PaletteCurrentColor";
            this.PaletteCurrentColor.Size = new System.Drawing.Size(48, 48);
            this.PaletteCurrentColor.TabIndex = 1;
            this.PaletteCurrentColor.Tag = "-1";
            this.PaletteCurrentColor.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Location = new System.Drawing.Point(3, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(370, 296);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.hullTabControl);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(362, 270);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Hulls";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // hullTabControl
            // 
            this.hullTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hullTabControl.Location = new System.Drawing.Point(3, 3);
            this.hullTabControl.Name = "hullTabControl";
            this.hullTabControl.SelectedIndex = 0;
            this.hullTabControl.Size = new System.Drawing.Size(356, 264);
            this.hullTabControl.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.turretButtonPanel);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(362, 270);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Turrets";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // turretButtonPanel
            // 
            this.turretButtonPanel.AutoScroll = true;
            this.turretButtonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.turretButtonPanel.Location = new System.Drawing.Point(3, 3);
            this.turretButtonPanel.Name = "turretButtonPanel";
            this.turretButtonPanel.Size = new System.Drawing.Size(356, 264);
            this.turretButtonPanel.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.Controls.Add(this.otherDeviceSpriteBox);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.flowLayoutPanel1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(362, 270);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Other Devices";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // otherDeviceSpriteBox
            // 
            this.otherDeviceSpriteBox.Assets = new FellSky.AssetManagement.SpriteAsset[0];
            this.otherDeviceSpriteBox.Location = new System.Drawing.Point(9, 26);
            this.otherDeviceSpriteBox.Name = "otherDeviceSpriteBox";
            this.otherDeviceSpriteBox.Size = new System.Drawing.Size(81, 92);
            this.otherDeviceSpriteBox.TabIndex = 3;
            this.otherDeviceSpriteBox.Value = null;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sprite:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.button4);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Controls.Add(this.button3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(96, 10);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(258, 254);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(3, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 40);
            this.button4.TabIndex = 3;
            this.button4.Text = "Power Generator";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(89, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Shield Emitter";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(175, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "Thruster";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 49);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 40);
            this.button3.TabIndex = 2;
            this.button3.Text = "SensorArray";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.propertyGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(376, 405);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Properties";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(370, 399);
            this.propertyGrid1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.checkedListBox1);
            this.tabPage6.Controls.Add(this.textBox1);
            this.tabPage6.Controls.Add(this.label1);
            this.tabPage6.Controls.Add(this.flowLayoutPanel2);
            this.tabPage6.Controls.Add(this.button9);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(376, 405);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Groups";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(8, 40);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(361, 124);
            this.checkedListBox1.TabIndex = 6;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(85, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(204, 20);
            this.textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Group Name:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.button8);
            this.flowLayoutPanel2.Controls.Add(this.button7);
            this.flowLayoutPanel2.Controls.Add(this.button6);
            this.flowLayoutPanel2.Controls.Add(this.button5);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 173);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(364, 106);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(3, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 39);
            this.button8.TabIndex = 8;
            this.button8.Text = "Select Inverse";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(84, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 39);
            this.button7.TabIndex = 7;
            this.button7.Text = "Select All";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(165, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 39);
            this.button6.TabIndex = 6;
            this.button6.Text = "Show Selected";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(246, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 39);
            this.button5.TabIndex = 5;
            this.button5.Text = "Hide Selected";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.Location = new System.Drawing.Point(295, 5);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 22);
            this.button9.TabIndex = 5;
            this.button9.Text = "Set";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // ShipDisplayControl
            // 
            this.ShipDisplayControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShipDisplayControl.Location = new System.Drawing.Point(0, 0);
            this.ShipDisplayControl.Name = "ShipDisplayControl";
            this.ShipDisplayControl.Size = new System.Drawing.Size(643, 431);
            this.ShipDisplayControl.TabIndex = 0;
            // 
            // loadXMLTemplateToolStripMenuItem
            // 
            this.loadXMLTemplateToolStripMenuItem.Name = "loadXMLTemplateToolStripMenuItem";
            this.loadXMLTemplateToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.loadXMLTemplateToolStripMenuItem.Text = "Load XML Template...";
            // 
            // ShipEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 455);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ShipEditorForm";
            this.Text = "ShipEditorForm";
            this.Load += new System.EventHandler(this.LoadForm);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorAlpha)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newShipToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem loadArchetypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFromFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsArchetypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem testDriveToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel ShipDisplayControl;
        private System.Windows.Forms.ToolStripMenuItem revertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem bringForwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bringBackwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToFrontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToBackToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button colorDesaturateBtn;
        private System.Windows.Forms.Button colorSaturateBtn;
        private System.Windows.Forms.Button colorLightenBtn;
        private System.Windows.Forms.Button colorDarkenBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown colorAlpha;
        public System.Windows.Forms.Button PaletteColor16;
        public System.Windows.Forms.Button PaletteColor8;
        public System.Windows.Forms.Button PaletteColor12;
        public System.Windows.Forms.Button PaletteColor4;
        public System.Windows.Forms.Button PaletteColor14;
        public System.Windows.Forms.Button PaletteColor6;
        public System.Windows.Forms.Button PaletteColor10;
        public System.Windows.Forms.Button PaletteColor2;
        public System.Windows.Forms.Button PaletteColor15;
        public System.Windows.Forms.Button PaletteColor7;
        public System.Windows.Forms.Button PaletteColor11;
        public System.Windows.Forms.Button PaletteColor3;
        public System.Windows.Forms.Button PaletteColor13;
        public System.Windows.Forms.Button PaletteColor5;
        public System.Windows.Forms.Button PaletteColor9;
        public System.Windows.Forms.Button PaletteColor1;
        public System.Windows.Forms.Button PaletteCurrentColor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectInverseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectSameGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem translateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem snapToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cloneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mirrorAlongXAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mirrorAlongYAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showGridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawPhysicsDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newStationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetCameraToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem hideSelectedPartsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllHiddenPartsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.TabControl hullTabControl;
        private System.Windows.Forms.ToolTip formToolTip;
        private System.Windows.Forms.FlowLayoutPanel turretButtonPanel;
        private Utilities.SpriteDropdown otherDeviceSpriteBox;
        private System.Windows.Forms.ToolStripMenuItem loadXMLTemplateToolStripMenuItem;
    }
}